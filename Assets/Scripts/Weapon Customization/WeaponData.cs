using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponData : MonoBehaviour 
{

    static WeaponData _instance;
    public static WeaponData Instance { get; private set; }
    //{
    //    get
    //    {
    //        if (_instance == null)
    //        {
    //            _instance = new WeaponData();
    //            _instance.customized_weapon_L = new WeaponSet();
    //            _instance.customized_weapon_L.side = WingType.LEFT;
    //            _instance.customized_weapon_R = new WeaponSet();
    //            _instance.customized_weapon_R.side = WingType.RIGHT;

    //        }
    //        return _instance;
    //    }
    //}

    public static bool HasNoWeapon
    {
        get
        {
            return Instance.customized_weapon_L.Count == 0 && Instance.customized_weapon_R.Count == 0;
        }
    }


    public WeaponSet customized_weapon_L;
    public WeaponSet customized_weapon_R;



    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void LoadWeaponData()
    {
        customized_weapon_L.LoadWeaponData();
        customized_weapon_R.LoadWeaponData();
    }
    public void SaveWeaponData()
    {
        customized_weapon_L.SaveWeaponData();
        customized_weapon_R.SaveWeaponData();
    }
    public void LoadInitialWeaponSet()
    {
        customized_weapon_L.LoadInitialWeaponSet();
        customized_weapon_R.LoadInitialWeaponSet();
    }
}