using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FRIA;
public static class LevelDataController
{
    public const int scene_number_offset = 3;
    public const int level_count = 40;

    //public static bool resetLevels = false;
    //public static bool testing = false;
    //public static int levelToTest = 1;
    //public static float player_weapon_part_damage_multiplier = 0.0f;

    public static HardData<int> _currentFakeLevel;
    public static int currentFakeLevel
    {
        get
        {
            if (_currentFakeLevel == null) _currentFakeLevel = new HardData<int>("CURRENT_SHOW_LEVEL_INDEX",1);
            return _currentFakeLevel.value;
        }
        set
        {
            if (_currentFakeLevel == null) _currentFakeLevel = new HardData<int>("CURRENT_SHOW_LEVEL_INDEX", 1);
            _currentFakeLevel.value = value;
        }
    }

    
    public static int CalculateNextLevel(int step)
    {
        currentFakeLevel += step;

        if (currentFakeLevel + scene_number_offset >= SceneManager.sceneCountInBuildSettings)
        {
            int levelToLoad = ((currentFakeLevel) % (SceneManager.sceneCountInBuildSettings - 1 - scene_number_offset));

            if (levelToLoad == 0)
            {
                levelToLoad = SceneManager.sceneCountInBuildSettings - 1;

                Debug.Log("Actual Level " + (levelToLoad - scene_number_offset).ToString());

                return levelToLoad;
            }

            Debug.Log("Actual Level " + (levelToLoad).ToString());

            return levelToLoad + scene_number_offset;
        }
        else
        {
            return currentFakeLevel + scene_number_offset;
        }
    }

    public static int CalculateLevelRotation()
    {
        if (currentFakeLevel >= level_count)
        {
            int level = ((currentFakeLevel) % (level_count));

            if (level == 0)
            {
                level = level_count;

                return level;
            }

            return level;
        }
        else
        {
            return currentFakeLevel;
        }
    }


}

