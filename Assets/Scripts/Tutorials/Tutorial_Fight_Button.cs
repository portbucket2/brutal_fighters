using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial_Fight_Button : MonoBehaviour
{
    public UnityEvent events;

    private void Start()
    {
        if (LevelDataController.currentFakeLevel == 2 && GameData.Instance.player_knows_crafting == 1 && GameData.Instance.player_knows_weapon_buying == 1)
        {
            events?.Invoke();
        }
    }
}
