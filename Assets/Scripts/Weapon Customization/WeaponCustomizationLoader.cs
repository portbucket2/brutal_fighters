using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class WeaponCustomizationLoader
{

    private List<WeaponPart> spawned_ui_parts = new List<WeaponPart>();

    public WeaponPart last_deployed_part;

    public int PartCount => spawned_ui_parts.Count;
    public void AddPart(WeaponPart part)
    {

        if (last_deployed_part)
        {
            last_deployed_part.rotator.onRotationUpdated -= ApplyWeapon;
        }

        last_deployed_part = part;

        part.rotator.onRotationUpdated += ApplyWeapon;




        spawned_ui_parts.Add(part);
        ApplyWeapon();
    }
    
    //public void RemovePart(WeaponPart part)
    //{
    //    if (HasPart(part))
    //    {
    //        spawned_ui_parts.Remove(part);
    //        ApplyWeapon();
    //    }
    //}
    //public void RemoveAllParts()
    //{
    //    spawned_ui_parts.Clear();
    //}
    //public bool HasPart(WeaponPart part)
    //{
    //    return spawned_ui_parts.Contains(part);
    //}
    public WeaponPart GetLastPart()
    {
        if (spawned_ui_parts.Count > 0)
        {
            return spawned_ui_parts[spawned_ui_parts.Count - 1];
        }
        else return null;
    }


    public float max_weight;
    public SpriteRenderer panel_fill;
    public GameObject panel_mask;
    public Animator full_load_indicator;
    public Transform root_base;

    private float cumulative_weight;
    private Vector3 panel_target = new Vector3(0, 1, 1);

    private DraggableWeaponPartPool weapon_pool;
    private WeaponCustomization customizer;

    WeaponSet weaponSet;
    public void Init(WeaponSet weaponSet, WeaponCustomization customizer)
    {
        this.customizer = customizer;
        this.weaponSet = weaponSet;
        weapon_pool = MonoBehaviour.FindObjectOfType<DraggableWeaponPartPool>();
        if (GameData.Instance.player_knows_crafting.value == 0)
        {
            WeaponPart default_weapon_data = weaponSet.defaultLevelWeapons[LevelDataController.CalculateLevelRotation() - 1];
            //LoadPart(new SpawnWeaponData(default_weapon_data), true);

        }
        else
        {
            for (int i = 0; i < weaponSet.spawn_data.Count; i++)
            {
                SpawnWeaponData wdata = weaponSet.spawn_data[i];
                LoadPart(wdata, i == 0);
            }
        }
        panel_mask.transform.localScale = panel_target;

    }

    void LoadPart(SpawnWeaponData wdata, bool firstIndex)
    {
        WeaponPart spawned_part = weapon_pool.GetWeaponPartFromID(wdata.id);

        spawned_part.transform.SetParent(root_base);
        spawned_part.transform.localScale = Vector3.one;
        spawned_part.transform.localPosition = wdata.position;
        spawned_part.transform.localEulerAngles = Vector3.forward * wdata.rotation;
        spawned_part.weapon_attach_check.SetConnected(true);

        spawned_ui_parts.Add(spawned_part);

        if (firstIndex)
        {
            spawned_part.GetComponent<DragToMove>().drag_enabled = false;
            spawned_part.GetComponent<ObjectSelector>().can_rotate = false;
            spawned_part.weapon_attach_check.is_first_part = true;
        }

        AddToWeight(spawned_part.GetWeight());
    }

    public bool AddToWeight(float value)
    {
        customizer.ShowNeedWeapon(false);

        if (cumulative_weight + value > max_weight)
        {
            full_load_indicator.Play("Shake", -1, 0);
            return false;
        }


        cumulative_weight += value;
        panel_target.x = cumulative_weight / max_weight;

        return true;
    }


    public void ResetWeapon()
    {
        if (spawned_ui_parts.Count == 0)
        {
            return;
        }
        spawned_ui_parts.Clear();

        ApplyWeapon();
        SceneManager.LoadScene(1);
    }

    public void UpdateFillBar()
    {
        panel_mask.transform.localScale = Vector3.Lerp(panel_mask.transform.localScale, panel_target, Time.deltaTime * 5);
        panel_fill.color = customizer.panel_color.Evaluate(panel_mask.transform.localScale.x);
    }
    public void ApplyWeapon()
    {

        weaponSet.spawn_data.Clear();
        foreach (WeaponPart weapon_part in spawned_ui_parts)
        {
            weapon_part.connected = weapon_part.weapon_attach_check.is_connected;

            weapon_part.position = weapon_part.transform.localPosition;
            weapon_part.rotation = weapon_part.transform.localEulerAngles.z;

            weaponSet.spawn_data.Add(new SpawnWeaponData(weapon_part));
        }
        weaponSet.SaveWeaponData();
    }
}
