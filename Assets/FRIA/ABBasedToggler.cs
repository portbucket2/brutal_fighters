using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if POTATO_AB_TEST
using PotatoSDK;
#endif

public class ABBasedToggler : MonoBehaviour
{
    [SerializeField] private ABToggleType enableType;
    private void OnEnable()
    {
        if (!ABToggleHub.IsEnabled(enableType)) gameObject.SetActive(false);   
    }
}

public static class ABToggleHub
{
    private static bool interstitialIsUnlocked = false;
    public static void UnlockInterstitial()
    {
        interstitialIsUnlocked = true;
    }
    public static bool IsEnabled(ABToggleType type)
    {
#if !POTATO_AB_TEST
        return false;
#else
        switch (type)
        {
            default:
                throw new System.Exception("AB toggle condition not defined");
            case ABToggleType.STAR_SYSTEM:
                {
                    return false;// BuildSpecManager.enableUpdates_060;
                }
            case ABToggleType.ENERGY_SYSTEM:
                {
                    return false;
//#if UNITY_IOS

//                    if (!ABMan.Instance) return false;
//                    switch (ABMan.GetValue_Int(ABtype.AB1_rpi_test2))
//                    {
//                        default:
//                        case 0:
//                            return true;
//                        case 1:
//                            return false;
//                    }
//#elif UNITY_ANDROID
//                    if (!BuildSpecManager.enableUpdates_060) return false;
//                    return false;
//                    //if (!ABMan.Instance) return false;
//                    //switch (ABMan.GetValue_Int(ABtype.AB0_rpi_test))
//                    //{
//                    //    default:
//                    //    case 0:
//                    //        return true;
//                    //    case 1:
//                    //        return false;
//                    //    case 2:
//                    //        return true;
//                    //}
//#endif

                }
            case ABToggleType.INTERSTITIAL_AD:
                {
#if UNITY_IOS
                    return true;
#elif UNITY_ANDROID
                    if (!ABMan.Instance) return true;
                    switch (ABMan.GetValue_Int(ABtype.AB4_ad_aggression_test1))
                    {
                        default:
                        case 0:
                            return true;
                        case 1:
                            return interstitialIsUnlocked;
                    }
#endif

                }
            case ABToggleType.ADS_ON_FAIL:
                {
#if UNITY_IOS
                    return true;
#elif UNITY_ANDROID
                    if (!ABMan.Instance) return true;
                    switch (ABMan.GetValue_Int(ABtype.AB4_ad_aggression_test1))
                    {
                        default:
                            return true;
                        case 2:
                            return false;
                    }
#endif
                }
            case ABToggleType.ADS_ON_FIRST3:
                {
#if UNITY_IOS
                    return true;
#elif UNITY_ANDROID
                    if (!ABMan.Instance) return true;
                    switch (ABMan.GetValue_Int(ABtype.AB4_ad_aggression_test1))
                    {
                        default:
                            return true;
                        case 1:
                            return false;
                    }
#endif
                }
        }
#endif
    }
}

public enum ABToggleType
{
    ENERGY_SYSTEM = 0,
    STAR_SYSTEM = 1,
    INTERSTITIAL_AD = 10,
    ADS_ON_FIRST3 = 11,
    ADS_ON_FAIL = 12,

}