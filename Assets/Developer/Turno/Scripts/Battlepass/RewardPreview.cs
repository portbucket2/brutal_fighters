using FRIA;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using Text = TMPro.TextMeshProUGUI;

public class RewardPreview : MonoBehaviour
{
    public ShopItemSO itemData;
    public Image itemPreview;
    public GameObject lockObject;
    public Button clickable;
    public Text lockedId;
    public Text unlockedId;
    public ParticleSystem particleSystem;
    public static UnityAction<Sprite> onSelectionAction;
    public static UnityAction<int, Sprite> trophyAchievedAction;

    private int index;

    private void OnEnable()
    {
        Currency.coinMan.AddListner_BalanceChanged(CurrencyType.XP, OnTrophyChange);
        clickable.onClick.AddListener(OnSelected);
    }

    

    private void OnDisable()
    {
        Currency.coinMan.RemoveListner_BalanceChanged(CurrencyType.XP, OnTrophyChange);
        clickable.onClick.RemoveListener(OnSelected);
    }
    private void OnSelected()
    {
        onSelectionAction?.Invoke(itemData.GetIcon);
    }
    private void OnTrophyChange()
    {
        if (!itemData.IsPurchased &&
           Currency.Balance(CurrencyType.XP) >=
           itemData.GetTrophyRequiredToUnlock)
        {
            itemData.SetPurchased();
            
            Unlock();
            trophyAchievedAction?.Invoke(index, itemData.GetIcon);
            StartCoroutine(InvokeWithDelay(1.528f));
            if (!particleSystem.isPlaying)
                particleSystem.Play();
        }
    }
    IEnumerator InvokeWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Global.ItemUnlockedAction?.Invoke(itemData.GetIcon);
    }
    public void LoadContent(ShopItemSO item, int index)
    {
        itemData = item;
        itemPreview.sprite = item.GetIcon;
        lockObject.SetActive(!itemData.IsPurchased);
        lockedId.text = (index + 1).ToString();
        unlockedId.text = (index + 1).ToString();
        this.index = index;
    }

    public void Unlock()
    {
        Centralizer.Add_DelayedAct(() =>
        {
            lockObject.SetActive(false);
        }, 0.756f);
       
    }
}
