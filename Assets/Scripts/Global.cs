using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Global : MonoBehaviour
{
    public static Global Instance { get; private set; }

    public static Action<bool> ShowBloodAction;
    public static Action NotEnoughCoinAction;
    public static Action<Sprite> ItemUnlockedAction;
    public static Action InAppReviewAction;
    public static Action<Vector3> CoinMinusAction;
    public static Action<Vector3> CoinPlusAction;
    public static Action CoinChangedAction;
    public static Action CloseButtonAction;

    public List<GameObject> deactivateOnAwake;

    public List<WeaponPart> weaponPartsUI;


    [SerializeField] GameObject bgOverlay;
    [Space]
    [SerializeField] GameObject notEnoughCoinPopup;
    [SerializeField] Animator notEnoughCoinAnimator;
    [Space]
    [SerializeField] Image itemIcon;
    [SerializeField] GameObject itemUnlockedPopup;
    [SerializeField] Animator itemUnlockedAnimator;
    public GameObject itemTutorialTapHere;
    [Space]
    [SerializeField] GameObject rateUsPopup;
    [SerializeField] Animator rateUsAnimator;
    [Space]
    [SerializeField] UIParticleSystem coinExplosion;
    [SerializeField] UIParticleSystem coinEarn;
    private Vector3 defaultPos = Vector3.zero;

    public Text debugText;

    private NativeInAppReview nativeInAppReview;

    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);

        foreach (GameObject item in deactivateOnAwake)
        {
            item.SetActive(false);
        }

        nativeInAppReview = GetComponent<NativeInAppReview>();

        NotEnoughCoinAction += ShowNotEnoughCoinPopup;
        ItemUnlockedAction += ShowItemUnlockedPopup;
        InAppReviewAction += ShowRateUs;
        CoinMinusAction += ExplodeCoin;
        CoinPlusAction += EarnCoin;

        CoinMinusAction += CoinChanged;
        CoinPlusAction += CoinChanged;
    }


    private void ShowRateUs()
    {
#if !UNITY_EDITOR
        bgOverlay.SetActive(true);
        rateUsPopup.SetActive(true);
        rateUsAnimator.Play("Popup", -1, 0);
#endif
    }

    public void RateUsYesPressed()
    {
        nativeInAppReview.RequestNativeInAppReview();
        
        InAppReviewPressedOnce();
    }

    public void CloseButtonPressed()
    {
        CloseButtonAction?.Invoke();
    }

    public void InAppReviewPressedOnce()
    {
        GameData.Instance.in_app_review_pressed_once.value = 1;
    }


    private void CoinChanged(Vector3 v3)
    {
        CoinChangedAction?.Invoke();
    }

    private void ShowNotEnoughCoinPopup()
    {
        bgOverlay.SetActive(true);
        notEnoughCoinPopup.SetActive(true);
        notEnoughCoinAnimator.Play("Popup", -1, 0);
    }

    private void ShowItemUnlockedPopup(Sprite sprite)
    {
        itemIcon.sprite = sprite;
        bgOverlay.SetActive(true);
        itemUnlockedPopup.SetActive(true);
        itemUnlockedAnimator.Play("Popup", -1, 0);
    }


    [Button("ExplodeCoinTest")]
    public bool ec;
    public void ExplodeCoinTest()
    {
        coinExplosion.Play();
    }

    public void ExplodeCoin(Vector3 pos)
    {
        coinExplosion.transform.position = pos;
        coinExplosion.Play();
    }

    [Button("EarnCoinTest")]
    public bool earnc;
    public void EarnCoinTest()
    {
        coinEarn.Play();
    }
    public void EarnCoin(Vector3 target)
    {
        Vector3 direction = (target - coinEarn.transform.position);
        coinEarn.EmissionDirection = direction.normalized;
        coinEarn.Lifetime = direction.magnitude / 1850f;
        coinEarn.Play();
    }

    [Button("CoinPlus")]
    public bool coinplus;

    public void CoinPlus()
    {
        Currency.Transaction(CurrencyType.COIN, 1000);
    }


    public void DebugText(string t)
    {
        debugText.text = t;
    }
}
