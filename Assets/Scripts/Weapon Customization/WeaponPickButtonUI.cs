using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WeaponPickButtonUI : WeaponPickButton, IPointerUpHandler, IPointerClickHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public void OnPointerClick(PointerEventData pointerEventData)
    {
        //Debug.LogError(name + " Clicked!");
        OnMouseDown();
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        OnMouseUp();
    }


    public void OnDrag(PointerEventData data)
    {
        //Debug.LogError(name + " Dragging!");
        OnMouseDrag();

    }

    public void OnBeginDrag(PointerEventData data)
    {
        DragStarted();
    }

    public void OnEndDrag(PointerEventData data)
    {
        DragEnded();
    }

}
