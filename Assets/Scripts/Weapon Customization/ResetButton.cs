using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetButton : MonoBehaviour
{
    public bool is_left;

    private WeaponCustomization weapon_customization;
    void Start()
    {
        weapon_customization = FindObjectOfType<WeaponCustomization>();
    }

    private void OnMouseDown()
    {
        if (GameData.Instance.player_knows_crafting.value == 0)
        {
            CustomEventTracker.FirstTimeCrafting();
            
            GameData.Instance.player_knows_crafting.value = 1;
        }


        if (is_left)
        {
            weapon_customization.leftLoader.ResetWeapon();
        }
        else
        {
            weapon_customization.rightLoader.ResetWeapon();
        }


    }
}
