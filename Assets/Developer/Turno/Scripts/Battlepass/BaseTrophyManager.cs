using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
using TMPro;
using Text = TMPro.TextMeshProUGUI;

public abstract class BaseTrophyManager : MonoBehaviour
{
    [Header("Slider Settings")]
    [Space(5.0f)]
    public Slider slider;
    public ScrollRect scrollRect;
    public AnimationCurve sliderCurve;
    public float lerpDuration = 2f;

    [Header("Trophy Settings")]
    [Space(5.0f)]
    public TrophyTracker trophyTracker;
    public ShopItemKeeper collection;
    public Text nextUnlockable;

    [Header("Preview Settings")]
    [Space(5.0f)]
    public Image itemPreview;

    [Header("Scene Settings")]
    [Space(5.0f)]
    public Button nextButton;

    [Header("Content Settings")]
    [Space(5.0f)]
    [SerializeField] protected GameObject emptyRewardPrefab;
    [SerializeField] protected GameObject rewardPrefab;
    [SerializeField] protected Transform contentHolder;
    [SerializeField] protected Vector2 contentTargetPosition;
    [SerializeField] protected Vector2 contentUpperLimit;
    [SerializeField] protected Vector2 contentLowerLimit;
    [SerializeField] protected float contentAnimationSpeed = 2;


    public abstract void Populate();
    public abstract void UpdateScrollPosition();
    public abstract void UpdateSlider();

}
