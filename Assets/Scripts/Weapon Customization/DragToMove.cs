using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragToMove : MonoBehaviour
{
    //public Text debug_text;
    private Vector3 touch_offset = Vector3.zero;

    private float touch_z_coord;

    private Vector3 start_pos = Vector3.zero;

    Camera cam;

    private Rigidbody2D rb;

    private WeaponCustomization weapon_customization;
    private WeaponPart weapon_part;

    public bool drag_enabled = true;
    public WeaponCustomizationLoader wcl_left => weapon_customization.leftLoader;
    public WeaponCustomizationLoader wcl_right => weapon_customization.rightLoader;

    private void Awake()
    {
        cam = Camera.main;
        weapon_customization = FindObjectOfType<WeaponCustomization>();
        weapon_part = GetComponent<WeaponPart>();
        //rb = GetComponent<Rigidbody2D>();
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 touchpos = Input.mousePosition;

        touchpos.z = touch_z_coord;

        return cam.ScreenToWorldPoint(touchpos);
    }

    private void OnMouseDown()
    {
        touch_z_coord = cam.WorldToScreenPoint(transform.position).z;

        touch_offset = transform.position - GetMouseWorldPos();

        start_pos = transform.position;
    }

    private void OnMouseDrag()
    {
        if (!drag_enabled)
        {
            return;
        }

        transform.position = GetMouseWorldPos() + touch_offset;
        //rb.MovePosition(GetMouseWorldPos() + touch_offset);
    }

    private void OnMouseUp()
    {
        if (weapon_part.weapon_attach_check.is_connected)
        {
            if (weapon_part.weapon_attach_check.weapon_panel == WeaponPanel.left)
            {
                DoSomeThing(weapon_part, wcl_left, wcl_right);
            }
            else
            {
                DoSomeThing(weapon_part, wcl_right, wcl_left);
            }
            
        }
        else
        {
            transform.position = start_pos;

            //weapon_customization.weapon_parts_L.Remove(weapon_part);
            //weapon_customization.weapon_parts_R.Remove(weapon_part);
        }

    }

    void DoSomeThing(WeaponPart weapon_part, WeaponCustomizationLoader wcl_main, WeaponCustomizationLoader wcl_other)
    {
        //if (!wcl_main.spawned_ui_parts.Contains(weapon_part))
        //{
        //    if (wcl_main.AddToWeight(weapon_part.GetWeight()))
        //    {
        //        wcl_other.AddToWeight(-weapon_part.GetWeight());
        //    }
        //    else
        //    {
        //        transform.position = start_pos;
        //    }
        //    wcl_main.spawned_ui_parts.Add(weapon_part);
        //}

        //wcl_other.spawned_ui_parts.Remove(weapon_part);

        //gameObject.transform.SetParent(wcl_main.root_base);
    }

    //void Update()
    //{
    //    transform.localPosition -= touch_relative * drag_sensitivity;
    //}
}
