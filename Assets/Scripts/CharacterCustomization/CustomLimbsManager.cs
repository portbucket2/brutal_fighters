using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class CustomLimbsManager : MonoBehaviour
{
    public bool in_shop;

    public bool manually_customized;

    public List<CustomLimbsMapper> custom_limbs_mapper;
    public List<ManualLimbCustomization> manual_customization;
    private CharacterShop characterShop;

    //private CurrencyInteraction currency;
    void Start()
    {
        //currency = FindObjectOfType<CurrencyInteraction>();
        characterShop = FindObjectOfType<CharacterShop>();
        if (manually_customized)
        {
            foreach (ManualLimbCustomization item in manual_customization)
            {
                item.EquipSkin();
            }
        }
        else
        {
            foreach (CustomLimbsMapper item in custom_limbs_mapper)
            {
                item.EquipSkin();
            }
        }

        if (in_shop)
        {
            //ShopItemKeeper.onEquipedItemChanged += RefreshLimbs;
            ContentPreview.itemPurchasedAction += RefreshLimbs;
            characterShop.tabsUI.OnTabChange.AddListener(RefreshAllLimbs);
            ContentPreview.itemSelectedAction += RefreshLimbs;
        }

    }

    private void OnDestroy()
    {
        if (in_shop)
        {
            ContentPreview.itemPurchasedAction -= RefreshLimbs;
            ContentPreview.itemSelectedAction -= RefreshLimbs;
        }
    }
    public void TestSetUpperBodyLimbSize(int id)
    {
        for (int i = 0; i < 4; i++)
        {
            manual_customization[i].EquipSkin(id);
        }
    }



    public void RefreshAllLimbs(int tabId)
    {
        Dictionary<ShopItemType, HardData<int>> shopSelections = ShopItemKeeper.Instance.shopSelections;

        foreach (var item in shopSelections)
        {
            switch (item.Key)
            {
                case ShopItemType.ARM:
                    for (int i = 0; i < 4; i++)
                    {
                        custom_limbs_mapper[i].EquipSkin(item.Value);
                    }
                    break;

                case ShopItemType.LEG:
                    for (int i = 4; i < 8; i++)
                    {
                        custom_limbs_mapper[i].EquipSkin(item.Value);
                    }
                    break;

                case ShopItemType.HEAD:
                    for (int i = 8; i < 9; i++)
                    {
                        custom_limbs_mapper[i].EquipSkin(item.Value);
                    }
                    break;

                case ShopItemType.TORSO:
                    for (int i = 9; i < 11; i++)
                    {
                        custom_limbs_mapper[i].EquipSkin(item.Value);
                    }
                    break;

                default:
                    break;
            }
        }
    }


    public void RefreshLimbs(ShopItemSO shopItemSO)
    {
        ShopItemType type = shopItemSO.GetShopItemType;
        int id = shopItemSO.GetId;

        switch (type)
        {
            case ShopItemType.ARM:
                for (int i = 0; i < 4; i++)
                {
                    custom_limbs_mapper[i].EquipSkin(id);
                }
                break;

            case ShopItemType.LEG:
                for (int i = 4; i < 8; i++)
                {
                    custom_limbs_mapper[i].EquipSkin(id);
                }
                break;

            case ShopItemType.HEAD:
                for (int i = 8; i < 9; i++)
                {
                    custom_limbs_mapper[i].EquipSkin(id);
                }
                break;

            case ShopItemType.TORSO:
                for (int i = 9; i < 11; i++)
                {
                    custom_limbs_mapper[i].EquipSkin(id);
                }
                break;

            default:
                break;
        }
    }
}

[System.Serializable]
public class CustomLimbsMapper
{
    public LimbType limb_type;
    public CustomLimbRoot limb_root;
    public int equipped_limb_skin;

    public void EquipSkin()
    {
        equipped_limb_skin = ShopItemKeeper.GetLimbSelection(limb_type);

        if (limb_root.OutOfBounds(equipped_limb_skin))
        {
            return;
        }

        limb_root.SetCustomLimb(equipped_limb_skin);
    }

    public void EquipSkin(int id)
    {
        if (limb_root.OutOfBounds(id))
        {
            return;
        }

        limb_root.SetCustomLimb(id);
    }
}


[System.Serializable]
public class ManualLimbCustomization
{
    public CustomLimbRoot limb_root;
    [SerializeField, SpriteProperty] private ShopItemSO equipped_limb_skin;

    public void EquipSkin()
    {
        limb_root.SetCustomLimb(equipped_limb_skin.GetId);
    }

    public void EquipSkin(int id)
    {
        if (limb_root.OutOfBounds(id))
        {
            return;
        }

        limb_root.SetCustomLimb(id);
    }

}