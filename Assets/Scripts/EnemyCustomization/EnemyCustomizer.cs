using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCustomizer : MonoBehaviour
{
    public EnemyCustomizationManager enemyCustomizationManager;

    public List<CustomLimbsMapper> custom_limbs_mapper;

    private void Start()
    {
        for (int i = 0; i < 4; i++) // Upper body
        {
            int id = (int)enemyCustomizationManager.LevelWiseCustomEnemy[LevelDataController.CalculateLevelRotation() - 1].upperBody;
            custom_limbs_mapper[i].EquipSkin(id);
        }

        for (int i = 4; i < 8; i++) // Lower body
        {
            int id = (int)enemyCustomizationManager.LevelWiseCustomEnemy[LevelDataController.CalculateLevelRotation() - 1].lowerBody;
            custom_limbs_mapper[i].EquipSkin(id);
        }
        for (int i = 8; i < 9; i++) // Head
        {
            int id = (int)enemyCustomizationManager.LevelWiseCustomEnemy[LevelDataController.CalculateLevelRotation() - 1].head;
            custom_limbs_mapper[i].EquipSkin(id);
        }
        for (int i = 9; i < 11; i++) // Torso
        {
            int id = (int)enemyCustomizationManager.LevelWiseCustomEnemy[LevelDataController.CalculateLevelRotation() - 1].torso;
            custom_limbs_mapper[i].EquipSkin(id);
        }
    }
}
