using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyAIData", menuName = "ScriptableObjects/EnemyAIData", order = 1)]
public class EnemyAI : ScriptableObject
{
    [Header("Easy Difficulty :")]
    [Space(10f)]
    public int easy_attackDamage = 100;
    public float easy_nextAttackGapMin = 0.2f;
    public float easy_nextAttackGapMax = 0.5f;
    public int easy_attackAnimationMin = 1;
    [Range(2, 5)]
    public int easy_attackAnimationMax = 4;

    [Header("Easy Difficulty :")]
    [Space(10f)]
    public int mid_attackDamage = 100;
    public float mid_nextAttackGapMin = 0.2f;
    public float mid_nextAttackGapMax = 0.5f;
    public int mid_attackAnimationMin = 1;
    [Range(2, 5)]
    public int mid_attackAnimationMax = 4;

    [Header("Easy Difficulty :")]
    [Space(10f)]
    public int hard_attackDamage = 100;
    public float hard_nextAttackGapMin = 0.2f;
    public float hard_nextAttackGapMax = 0.5f;
    public int hard_attackAnimationMin = 1;
    [Range(2, 5)]
    public int hard_attackAnimationMax = 4;


    public List<HealthXDifficulty> healthXDifficulties;


    public List<HealthXDifficulty> GetList()
    {
        return healthXDifficulties;
    }
}
public enum DIFFICULTY
{
    EASY,
    MIDIUM,
    HARD
}

[System.Serializable]
public struct HealthXDifficulty
{
    public DIFFICULTY diffAt100;
    public DIFFICULTY diffAt70;
    public DIFFICULTY diffAt50;
    public DIFFICULTY diffAt20;
}

