using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPart : MonoBehaviour
{
    public Transform top;
    public int id;
    public Vector2 position;
    public float rotation;
    public float damage = 10;
    public bool connected;
    public WeaponAttachCheck weapon_attach_check;
    public float weight;

    public DragRotator rotator;

    //private List<float> weight = new List<float>() {1,1,1,2,2,2,3,3,3,1,1,1,3,3,3,3,3,3,3,3,3};
    public float GetWeight()
    {
        return weight;//[id];
    }

    //private void Awake()
    //{
    //    rotator.onRotationUpdated += () => 
    //    {

    //    };
    //}
}
