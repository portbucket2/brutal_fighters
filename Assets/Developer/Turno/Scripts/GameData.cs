using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using FRIA;

public class GameData
{
    public static GameData _instance;
    public static GameData Instance 
    {
        get
        {
            if (_instance == null) _instance = new GameData();
            return _instance;
        }
    }

    //public HardData<int> currency;

    public HardData<int> healthLevel;
    public HardData<int> damageLevel;
    public HardData<int> earningLevel;


    public HardData<int> player_knows_crafting;
    public HardData<int> player_knows_weapon_buying;
    public HardData<int> player_knows_limb_upgrade;

    public HardData<int> player_leaderboard_rank;

    public HardData<int> got_free_rv_coin_once;

    public HardData<int> in_app_review_pressed_once;
    //public int Currency
    //{
    //    get => currency;
    //    set => currency.value = value;
    //}



    public int Health
    {
        get 
        {
            return ConstantManager.initialHealth + ConstantManager.healthUpgradeBasicValue * healthLevel.value;
        }
    }
    public int Damage
    {
        get
        {
            return ConstantManager.initialDamage + ConstantManager.damageUpgradeBasicValue * damageLevel.value;
        }
    }
    public float EarningMultiplier
    {
        get
        {
            return ConstantManager.initialEarning + ConstantManager.earningUpgradeBasicValue * earningLevel.value;
        }
    }


    public int HealthUpgradeCost
    {
        get
        {
            return ConstantManager.initialhealthUpgradeCost + 250 * healthLevel.value;
        }
    }
    public int DamageUpgradeCost
    {
        get
        {
            return ConstantManager.initialDamageUpgradeCost + 250 * damageLevel.value;
        }
    }
    public int EarningUpgradeCost
    {
        get
        {
            return ConstantManager.initialCoinUpgradeCost + 250 * earningLevel.value;
        }
    }
    public void UpgradeAttributeNow(UpgradeType upgradeType)
    {
        switch (upgradeType)
        {
            case UpgradeType.HEALTH:
                if (Currency.Transaction(CurrencyType.COIN, -HealthUpgradeCost))
                {
                    healthLevel.value++;
                }
                break;
            case UpgradeType.DAMAGE:
                if (Currency.Transaction(CurrencyType.COIN, -DamageUpgradeCost))
                {
                    damageLevel.value++;
                }
                break;
            case UpgradeType.COIN:
                if (Currency.Transaction(CurrencyType.COIN, -EarningUpgradeCost))
                {
                    earningLevel.value++;
                }
                break;
        }
    }

    public GameData()
    {
        //currency = new HardData<int>("currency", ConstantManager.initialCurrency);
        healthLevel = new HardData<int>("healthLevel", 0);
        damageLevel = new HardData<int>("damageLevel", 0);
        earningLevel = new HardData<int>("earningLevel", 0);
        player_knows_crafting = new HardData<int>("PlayerKnowsCrafting", 0);
        player_knows_weapon_buying = new HardData<int>("PlayerKnowsWeaponBuying", 0);
        player_knows_limb_upgrade = new HardData<int>("PlayerKnowsLimbUpgrade", 0);
        player_leaderboard_rank = new HardData<int>("PlayerLeaderboardRank", 9984);
        got_free_rv_coin_once = new HardData<int>("GotFreeRVCoinOnce", 0);
        in_app_review_pressed_once = new HardData<int>("InAppReviewPressedOnce", 0);
    }


    //public void UpgradeHealth()
    //{
    //    Health += ConstantManager.healthUpgradeBasicValue;
    //}
    //public void UpgradeDamage()
    //{
    //    Damage += ConstantManager.damageUpgradeBasicValue;
    //}
    //public void IncreaseUpgradeMultiplier(float percentage)
    //{
    //    UpgradeMultiplier += percentage;
    //}
    //public void IncreaseHealthUpgradeCost()
    //{
    //    ChangeValue( HealthUpgradeCost, InteractionType.SUBTRACT);
    //    int additionalValue = 250;
    //    HealthUpgradeCost += additionalValue;
    //}
    //public void IncreaseDamageUpgradeCost()
    //{
    //    ChangeValue( DamageUpgradeCost, InteractionType.SUBTRACT);
    //    int additionalValue = 250;
    //    DamageUpgradeCost += additionalValue;
    //}
    //public void IncreaseCoinUpgradeCost()
    //{
    //    ChangeValue( CoinUpgradeCost, InteractionType.SUBTRACT);
    //    int additionalValue = 250;
    //    CoinUpgradeCost += additionalValue;
    //}
    //public void ChangeValue( int value, InteractionType type)
    //{
    //    if (CurrenyValidation( value, type))
    //    {
    //        if (type == InteractionType.ADD)
    //        {
    //            Currency += value;
    //        }
    //        else
    //        {
    //            Currency -= value;
    //        }

    //    }
    //}
    //public bool CurrenyValidation(int value, InteractionType type)
    //{
    //    if (type == InteractionType.SUBTRACT)
    //    {
    //        bool isValid = Currency < value ? false : true;
    //        return isValid;
    //    }
    //    else
    //    {
    //        return true;
    //    }

    //}

    public static void SaveDataForJSON(List<SpawnWeaponData> testData, int instanceId)
    {
        string jsonFile = JsonConvert.SerializeObject(testData, Formatting.Indented, new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        });
#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
        string deviceFileName = string.Format("/WeaponDataFile{0}", instanceId);
        File.WriteAllText(Application.persistentDataPath + deviceFileName, jsonFile);
#endif
    }

    public static List<SpawnWeaponData> LoadDataForJSON(int instanceId)
    {
        string fileName = string.Format("/WeaponDataFile{0}", instanceId);
        string dataPath = Application.persistentDataPath;

#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
        if (File.Exists(dataPath + fileName))
        {
            using (StreamReader reader = File.OpenText(dataPath + fileName))
            {
                JsonSerializer serializer = new JsonSerializer();
                List<SpawnWeaponData> weaponData = (List<SpawnWeaponData>)serializer.Deserialize(reader, typeof(List<SpawnWeaponData>));
                return weaponData;
            }
        }
        else
        {
            Debug.Log("<color=red> File doesn't exists </color>");
            return new List<SpawnWeaponData>();
        }
#endif

    }

}
public struct SpawnWeaponData
{
    public int id;
    public Vector2 position;
    public float rotation;
    public SpawnWeaponData(WeaponPart part) 
    {
        id = part.id;
        position = part.position;
        rotation = part.rotation; 
    }

}
public enum InteractionType
{
    ADD,
    SUBTRACT
}
//public sealed class GamePersistantData : MonoBehaviour
//{
//    private string path;
//    private void Start()
//    {
//#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
//        path = Application.persistentDataPath;
//#endif 
//    }

//}
