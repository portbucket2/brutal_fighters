using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantManager : MonoBehaviour
{
    public const string currentLevelNumber = "currentLevelNumber";
    public const string healthUpgradeCost = "healthUpgradeCost";
    public const string damageUpgradeCost = "damageUpgradeCost";
    public const string coinUpgradeCost = "coinUpgradeCost";
    public const string purchaseStoragePath = "purchaseStoragePath";
    public const string GAME_NAME = "Brutal Fighters";

    public const int initialCurrency = 0;
    public const int initialTrophy = 0;
    public const int perLevelCurrencyEarning = 25;
    public const int perLevelTrophyEarning = 5;
    public const int initialHealth = 100;
    public const int healthUpgradeBasicValue = 10;
    public const int initialDamage = 10;
    public const float initialEarning = 1.5f;
    public const int initialhealthUpgradeCost = 100;
    public const int initialDamageUpgradeCost = 100;
    public const int initialCoinUpgradeCost = 100;
    public const int damageUpgradeBasicValue = 2;
    public const float earningUpgradeBasicValue = 0.5f;

    public const int TOTAL_GAME_LEVELS = 1;
}
