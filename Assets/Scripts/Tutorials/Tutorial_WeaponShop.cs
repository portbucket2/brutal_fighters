using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial_WeaponShop : Tutorial
{
    public UnityEvent teach_to_buy;
    public UnityEvent after_purchase;

    private void Start()
    {
        InvokeLevelSpecificEvents();

        if (GameData.Instance.player_knows_weapon_buying.value == 0)
        {
            ContentPreview.weaponPurchasedAction += OnItemPurchase;

            teach_to_buy?.Invoke();
        }
        else
        {
            if (LevelDataController.currentFakeLevel == 2)
            {
                after_purchase?.Invoke();
            }
        }

    }

    private void OnDestroy()
    {
        ContentPreview.weaponPurchasedAction -= OnItemPurchase;
    }

    private void OnItemPurchase(ShopItemSO itemData)
    {
        GameData.Instance.player_knows_weapon_buying.value = 1;

        after_purchase?.Invoke();

        InvokeLevelSpecificEvents();
    }
}
