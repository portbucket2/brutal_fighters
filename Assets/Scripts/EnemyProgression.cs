using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProgression : MonoBehaviour
{
    public float base_health_multiplier;
    public float damage_multiplier;

    public int levels_count = 40;

    private HealthManager health_manager;

    public List<GameObject> weapon_L;
    public List<GameObject> weapon_R;

    public List<LightSaber> weapon_root;

    public List<float> level_wise_weapon_damage;

    public List<float> level_wise_health;

    private int idx_for_level = 0;
    
    private void Awake()
    {
        health_manager = FindObjectOfType<HealthManager>();

        idx_for_level = LevelDataController.CalculateLevelRotation() - 1;

        //Debug.LogError("Current Actual Level: " + (idx_for_level + 1));
    }

    private void Start()
    {
        //health_manager.max_health = level_wise_health[idx_for_level] * base_health_multiplier;
        //health_manager.health_bar.max_health = health_manager.max_health;
        //health_manager.health_bar.Initiate();

        //Debug.LogError(health_manager.max_health);

        foreach (LightSaber weapon in weapon_root)
        {
            weapon.attack_damage = level_wise_health[idx_for_level] * damage_multiplier;
        }

        weapon_L[idx_for_level].SetActive(true);
        weapon_R[idx_for_level].SetActive(true);

        for (int i = 0; i < weapon_R[idx_for_level].transform.childCount; i++)
        {
            weapon_R[idx_for_level].transform.GetChild(i).GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 4;
        }

        //Debug.LogError(weapon_L[idx_for_level].activeInHierarchy);
    }
}
