using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using TMPro;
using Text = TMPro.TextMeshProUGUI;
using UnityEngine.SceneManagement;
using System;
using FRIA;

public class ShopManager : MonoBehaviour
{
    public static ShopManager Instance { get; private set; }
    public Transform rootObject;

    public List<Transform> TabContent = new List<Transform>();
    public ShopItemKeeper itemCollection;
    public GameObject previewContentObject;
    [Header("Purchase Preview")]
    public Image selectedPreview;
    public Text selectedItemCost;
    public Button purchaseButton;
    public Button equipNowButton;
    public Button purchaseUpgradeButton;
    public Button playButton;
    public Text selectedUpgradeCostText;
    public Text weaponDamageText;

    [SerializeField] private ShopItemSO selectedItem;
    private GameObject purchasedWeaponPriceHolder;
    private const string purchaseStatus = "Purchased";
    [Header("Upgrade Section")]
    public int globalUpgradeCost = 0;
    public Text globalUpgradeText;
    public Text globalUpgradeTypeText;
    //public Text damageUpgradeText;
    //public Text costUpgradeText;
    private UpgradeType type;
    public int currentTabIndex;

    public TabsUIHorizontal tabsUI;
    private const string healthString = "Health";
    private const string damageString = "Damage";
    private const string costString = "Coin Multiplier";
    private void Awake()
    {
        itemCollection.EnsureInit();
        Instance = this;
    }

    private void Start()
    {
        ContentPreview.itemSelectedListener += OnItemSelected;
        //UpgradePreview.upgradeSelectedListener += OnUpgradeSelected;
        ShopItemKeeper.onEquipedItemChanged += EquippedLimbChanged;
        tabsUI.OnTabChange.AddListener(TabChanged);
        currentTabIndex = 0;
        purchaseButton.onClick.AddListener(PurchaseWeapon);
        //purchaseUpgradeButton.onClick.AddListener(PurchaseUpgrade);
        equipNowButton.onClick.AddListener(EquipLimb);
        playButton.onClick.AddListener(Play);
        PopulateTabContent();
        TabContent[0].GetChild(0).GetComponent<Button>().onClick.Invoke();
        //globalUpgradeText.text = currencyInteraction.persistantData.LoadData().Health.ToString();
        //damageUpgradeText.text = currencyInteraction.gameData.LoadData().Damage.ToString();
        //costUpgradeText.text = currencyInteraction.gameData.LoadData().UpgradeMultiplier.ToString();
    }

    

    private void OnDestroy()
    {    
        ContentPreview.itemSelectedListener -= OnItemSelected;
        //UpgradePreview.upgradeSelectedListener -= OnUpgradeSelected;
        ShopItemKeeper.onEquipedItemChanged -= EquippedLimbChanged;
        tabsUI.OnTabChange.RemoveListener(TabChanged);
        purchaseButton.onClick.RemoveListener(PurchaseWeapon);
        //purchaseUpgradeButton.onClick.RemoveListener(PurchaseUpgrade);
    }
    private void HideSelectedHighlightForAll()
    {
        for (int i = 0; i < TabContent[currentTabIndex].childCount; i++)
        {
            TabContent[currentTabIndex].GetChild(i).GetComponent<ContentPreview>().SetSelectionHighlightStatus(false);
        }
    }
    private void OnItemSelected(ShopItemSO itemSO, ContentPreview contentPreview)
    {
        purchaseButton.interactable = true;
        selectedItem = itemSO;
        if(!itemSO.isSelectable) selectedPreview.sprite = itemSO.GetIcon;
        selectedItemCost.text = itemSO.GetCost.ToString();
        purchasedWeaponPriceHolder = contentPreview.PriceHolder;
        HideSelectedHighlightForAll();
        contentPreview.SetSelectionHighlightStatus(true);

        if (!itemSO.IsPurchased)
        {
            purchaseButton.gameObject.SetActive(true);
            if(itemSO.isSelectable) equipNowButton.gameObject.SetActive(false);

        }
        else
        {
            purchaseButton.gameObject.SetActive(false);
            if (!itemSO.isSelectable)
            {
                equipNowButton.gameObject.SetActive(false);
                
            }
            else
            {
                int id = ShopItemKeeper.GetEquipedID(itemSO.GetShopItemType);
                equipNowButton.gameObject.SetActive(id != itemSO.GetId);
            }
        }


        weaponDamageText.text = itemSO.GetDamage.ToString();
        
    }
    private void TabChanged(int arg0)
    {
        
        currentTabIndex = arg0;
        //$"Pressed Tab {currentTabIndex}".Debug("FF00FF");
        switch (arg0)
        {
            case 0:
            case 1:
            case 2:
            case 7:
                TabContent[arg0].GetChild(0).GetComponent<Button>().onClick.Invoke();
                break;
            case 3:
                TabContent[arg0]
                    .GetChild(itemCollection.shopSelections[ShopItemType.ARM]).GetComponent<Button>().onClick.Invoke();
                //$"{TabContent[arg0].GetChild(itemCollection.shopSelections[ShopItemType.ARM])}".Debug("0000FF");
                TabContent[arg0].GetChild(itemCollection.shopSelections[ShopItemType.ARM])
                    .GetComponent<ContentPreview>().EquippedTick(true);
                break;
            case 4:
                TabContent[arg0]
                    .GetChild(itemCollection.shopSelections[ShopItemType.LEG]).GetComponent<Button>().onClick.Invoke();

                TabContent[arg0].GetChild(itemCollection.shopSelections[ShopItemType.LEG])
                    .GetComponent<ContentPreview>().EquippedTick(true);
                break;
            case 5:
                TabContent[arg0]
                    .GetChild(itemCollection.shopSelections[ShopItemType.HEAD]).GetComponent<Button>().onClick.Invoke();
                TabContent[arg0].GetChild(itemCollection.shopSelections[ShopItemType.HEAD])
                    .GetComponent<ContentPreview>().EquippedTick(true);
                break;
            case 6:
                TabContent[arg0]
                    .GetChild(itemCollection.shopSelections[ShopItemType.TORSO]).GetComponent<Button>().onClick.Invoke();
                TabContent[arg0].GetChild(itemCollection.shopSelections[ShopItemType.TORSO])
                    .GetComponent<ContentPreview>().EquippedTick(true);
                break;
        }

        CheckAllCost();
    }
    private void Play()
    {
        SceneManager.LoadScene(LevelDataController.CalculateNextLevel(0));
    }
    //private void OnUpgradeSelected(Image image, UpgradeType upgradeType)
    //{
    //    purchaseUpgradeButton.interactable = true;
    //    selectedPreview.sprite = image.sprite;
    //    type = upgradeType;

    //    switch (upgradeType)
    //    {
    //        case UpgradeType.HEALTH:
    //            selectedUpgradeCostText.text = currencyInteraction.gameData.HealthUpgradeCost.ToString();
    //            globalUpgradeCost = currencyInteraction.gameData.HealthUpgradeCost;
    //            globalUpgradeTypeText.text = healthString;
    //            globalUpgradeText.text = currencyInteraction.gameData.Health.ToString();
    //            break;
    //        case UpgradeType.DAMAGE:
    //            selectedUpgradeCostText.text = currencyInteraction.gameData.DamageUpgradeCost.ToString();
    //            globalUpgradeCost = currencyInteraction.gameData.DamageUpgradeCost;
    //            globalUpgradeTypeText.text = damageString;
    //            globalUpgradeText.text = currencyInteraction.gameData.Damage.ToString();
    //            break;
    //        case UpgradeType.COIN:
    //            selectedUpgradeCostText.text = currencyInteraction.gameData.CoinUpgradeCost.ToString();
    //            globalUpgradeCost = currencyInteraction.gameData.CoinUpgradeCost;
    //            globalUpgradeTypeText.text = costString;
    //            globalUpgradeText.text = currencyInteraction.gameData.UpgradeMultiplier.ToString();
    //            break;
    //    }

    //}
    private void EquippedLimbChanged(ShopItemType itemType, int id)
    {   
        
        //Debug.LogError($"{currentTabIndex}");
        for (int i = 0; i < TabContent[currentTabIndex].childCount; i++)
        {
            TabContent[currentTabIndex].GetChild(i)
           .GetComponent<ContentPreview>().EquippedTick(false);
        }
        TabContent[currentTabIndex].GetChild(itemCollection.shopSelections[itemType])
           .GetComponent<ContentPreview>().EquippedTick(true);


    }
    private void PurchaseWeapon()
    {
        if (!selectedItem.IsPurchased) // !currencyInteraction.purchaseHistory[selectedWeapon.GetId])
        {
            if(Currency.Balance(CurrencyType.COIN) >= selectedItem.GetCost)
            {
                selectedItem.SetPurchased();


                //currencyInteraction.purchaseHistory[selectedWeapon.GetId] = true;
                //currencyInteraction.SavePurchaseHistory(currencyInteraction.purchaseHistory);
                Currency.Transaction(CurrencyType.COIN, -selectedItem.GetCost);
                CheckAllCost();
                purchasedWeaponPriceHolder.SetActive(false);
                purchaseButton.gameObject.SetActive(false);
                if (selectedItem.isSelectable)
                {
                    itemCollection.OnEquip(selectedItem.GetShopItemType, selectedItem.GetId);
                }
            }
            else
            {
                selectedItemCost.text = "too poor";
            }
        }
        
        
    }
    private void EquipLimb()
    {
        if (selectedItem.IsPurchased) // !currencyInteraction.purchaseHistory[selectedWeapon.GetId])
        {
            itemCollection.OnEquip(selectedItem.GetShopItemType, selectedItem.GetId);
            equipNowButton.gameObject.SetActive(false);
            
        }
    }
    //private void PurchaseUpgrade()
    //{
    //    if (currencyInteraction.ValidatePurchase(globalUpgradeCost, InteractionType.SUBTRACT))
    //    {
    //        switch (type)
    //        {
    //            case UpgradeType.HEALTH:
    //                currencyInteraction.HealthUpgradeCostUpdate();
    //                globalUpgradeCost = currencyInteraction.gameData.HealthUpgradeCost;
    //                selectedUpgradeCostText.text = currencyInteraction.gameData.HealthUpgradeCost.ToString();
    //                currencyInteraction.UpgradeHealth();
    //                globalUpgradeText.text = currencyInteraction.gameData.Health.ToString();
    //                break;
    //            case UpgradeType.DAMAGE:
    //                currencyInteraction.DamageUpgradeCostUpdate();
    //                globalUpgradeCost = currencyInteraction.gameData.DamageUpgradeCost;
    //                selectedUpgradeCostText.text = currencyInteraction.gameData.DamageUpgradeCost.ToString();
    //                currencyInteraction.UpgradeDamage();
    //                globalUpgradeText.text = currencyInteraction.gameData.Damage.ToString();
    //                break;
    //            case UpgradeType.COIN:
    //                currencyInteraction.CoinUpgradeCostUpdate();
    //                globalUpgradeCost = currencyInteraction.gameData.CoinUpgradeCost;
    //                selectedUpgradeCostText.text = currencyInteraction.gameData.CoinUpgradeCost.ToString();
    //                currencyInteraction.SetUpgradeMultiplier(0.5f);
    //                globalUpgradeText.text = currencyInteraction.gameData.UpgradeMultiplier.ToString();
    //                break;
    //        }

    //    }
    //    else
    //    {
    //        selectedUpgradeCostText.text = "too poor";
    //    }
    //}
    private void PopulateTabContent()
    {
        var shopDictionary = itemCollection.shopDictionary;
        if (shopDictionary.Count == 0) Debug.LogError("Shop Dictionary is Empty");
        int i = 0;
        foreach (var list in shopDictionary)
        {
            foreach (ShopItemSO item in shopDictionary[list.Key])
            {
                ContentPreview content = Instantiate(previewContentObject, TabContent[i]).GetComponent<ContentPreview>();
                content.LoadData(item: item);
            }
            i++;
        }
    }

    private void CheckAllCost()
    {
        for (int i = 0; i < TabContent[currentTabIndex].childCount; i++)
        {
            //$"Items for {currentTabIndex} tab checked".Debug("C3C3C3");
            TabContent[currentTabIndex].GetChild(i)
           .GetComponent<ContentPreview>().CostTextChange();
        }
    }

}