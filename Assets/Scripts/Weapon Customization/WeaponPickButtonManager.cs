using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponPickButtonManager : MonoBehaviour
{
    public float pos_x = -40;

    RectTransform rt;


    [SerializeField] WeaponPickButton pickButtonPrefab;
    [SerializeField] RectTransform spawnRoot;
    [SerializeField] ScrollRect scrollRect;


    [SerializeField] List<WeaponPickButton> instantiatedButtons;
    [SerializeField] ShopItemKeeper shopItemKeeper;

    
    private void Awake()
    {
        if (!shopItemKeeper) shopItemKeeper = ShopItemKeeper.Instance;
        shopItemKeeper.EnsureInit();

        Load();

        //rt = GetComponent<RectTransform>();

        //IncreaseWidth();
    }
    public void IncreaseWidth()
    {
        rt.sizeDelta = new Vector2(pos_x + 100, 100);
    }

    void Load()
    {
        List<ShopItemSO> items = shopItemKeeper.GetWeaponCollection;

        foreach (ShopItemSO item in items)
        {
            GameObject go = Instantiate(pickButtonPrefab.gameObject);
            go.transform.SetParent(spawnRoot);
            go.transform.localScale = Vector3.one;
            go.transform.localRotation = Quaternion.identity;
            WeaponPickButton wpb = go.GetComponent<WeaponPickButton>();
            wpb.Load(item, scrollRect);
            instantiatedButtons.Add(wpb);
        }
    }
}
