using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingData : MonoBehaviour
{
    private List<SpawnWeaponData> weaponData = new List<SpawnWeaponData>();
    void Start()
    {
        weaponData.Add(new SpawnWeaponData { id = 0, position = new Vector2(5, 10), rotation = 9.5f });
        weaponData.Add(new SpawnWeaponData { id = 1, position = new Vector2(10, 15), rotation = 11.5f });
        GameData.SaveDataForJSON(weaponData, 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
