using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WeaponPartUIDetector : MonoBehaviour
{
    private WeaponCustomization weaponCustomization;

    [SerializeField] WeaponPanel weaponPanel;

    [SerializeField] UnityEvent OnEntered;
    [SerializeField] UnityEvent OnExited;

    void Awake()
    {
        weaponCustomization = FindObjectOfType<WeaponCustomization>();

        WeaponCustomization.WeaponPartPanelUpdated += OnWeaponPanelUpdated;
        WeaponCustomization.WeaponPartDroppedAction += WeaponDropped;
    }

    void OnWeaponPanelUpdated(WeaponPanel weaponPanel)
    {
        if (weaponPanel == this.weaponPanel)
        {
            OnEntered?.Invoke();
        }
        else
        {
            OnExited?.Invoke();
        }
    }

    void WeaponDropped()
    {
        OnExited?.Invoke();
    }
}
