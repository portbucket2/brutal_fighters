using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using FRIA;

[CreateAssetMenu(fileName = "New Shop Data", menuName = "Shop Data", order = 51)]
public class ShopItemSO : ScriptableObject
{
    #region Custom Variables
    public enum ItemUnlockMethod
    {
        Ads = 0,
        Coins = 1,
        Free = 2,
        Trophy = 3,
    }
    #endregion
    [SerializeField] private int id;
    [SerializeField] private string weaponName;
    [SerializeField] private int cost;
    [SerializeField] private int unlocksAtLevel;
    [SerializeField] private ItemUnlockMethod itemUnlockMethod;
    [SerializeField] private bool isDefault;
    [SerializeField, SpriteProperty] private Sprite icon;

    [SerializeField] private ShopItemType shopItemType;
    [SerializeField] private int damage;

    internal bool isSelectable;



    private HardData<bool> isPurchased;

    public bool IsDefault
    {
        get => isDefault;
    }
    public int GetTrophyRequiredToUnlock
    {
        get => (unlocksAtLevel - 1) * ConstantManager.perLevelTrophyEarning;
    }
    public int GetUnlockAtLevel
    {
        get => unlocksAtLevel;
    }
    public bool IsPurchased
    {
        get
        {
            if (isPurchased == null) isPurchased = new HardData<bool>($"SHOPITEM_PURCHASED_{shopItemType}_{id}",isDefault);
            return isPurchased.value;
        }
    }
    public void SetPurchased()
    {
        isPurchased.value = true;
    }
    public ItemUnlockMethod GetUnlockMethod
    {
        get => itemUnlockMethod;
    }
    public int GetId
    {
        get => id;
    }
    public string GetWeaponName
    {
        get => weaponName;
    }

    public Sprite GetIcon
    {
        get => icon;
    }
    public ShopItemType GetShopItemType
    {
        get => shopItemType;
    }
    public int GetCost
    {
        get => cost;
    }
    public int GetDamage
    {
        get => damage;
    }

}
//#if UNITY_EDITOR
//[CustomEditor(typeof(WeaponSO))]
//public class WeaponSOEditor : Editor
//{
//    public override Texture2D RenderStaticPreview(string assetPath, Object[] subAssets, int width, int height)
//    {
//        var weapon = (WeaponSO)target;
//        if (weapon == null || weapon.GetAsset == null)
//            return null;
//        var texture = new Texture2D(width, height);
//        EditorUtility.CopySerialized(weapon.GetAsset.texture, texture);
//        return texture;
//    }
//}

//#endif