using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public void LoadScene(int value)
    {
        SceneManager.LoadScene(value);
    }

    public void SaveWeaponData()
    {
        WeaponData.Instance.SaveWeaponData();
    }

    public void ContinueLevel(int step = 0)
    {
        SceneManager.LoadScene(LevelDataController.CalculateNextLevel(step));
    }
}
