using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial_HomeToCraft : Tutorial
{
    private void Awake()
    {
        if (GameData.Instance.player_knows_crafting == 0)
        {
            gameObject.SetActive(InvokeLevelSpecificEvents());
        }
    }
}
