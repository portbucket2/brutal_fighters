using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimbCustomizationPlayerModelBehavior : MonoBehaviour
{

    public List<RotationFollower> rotationFollow;

    public List<PositionFollower> positionFollow;

    private void FixedUpdate()
    {
        foreach (RotationFollower follower in rotationFollow)
        {
            follower.FollowRotation();
        }

        foreach (PositionFollower follower in positionFollow)
        {
            follower.FollowPosition();
        }
    }
}
