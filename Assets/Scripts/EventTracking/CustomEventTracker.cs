using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using LionStudios.Suite.Analytics;
using ByteBrewSDK;
using PotatoSDK;

public static class CustomEventTracker
{
    public static void CustomSDKEvent(string eventName, Dictionary<string, object> args = null)
    {
        LionAnalytics.LogEvent(eventName, args);
        GameAnalytics.NewDesignEvent(eventName, args);
        ByteBrew.NewCustomEvent(eventName, ParseEventValues(args));


#if UNITY_EDITOR
        string logString = "";
        
        logString = eventName;

        //if (args != null)
        //{
        //    foreach (KeyValuePair<string, object> item in args)
        //    {
        //        logString += "\t********\t" + item.Key + ", " + item.Value;
        //    }
        //}
        
        logString += "\t********\t" + ParseEventValues(args);

        Debug.Log("<color=cyan>*******************************\t" + logString + "</color>");
#endif
    }

    public static string ParseEventValues(Dictionary<string, object> values)
    {
        var parsedValueSTR = "";
        foreach (var keyPair in values)
        {
            parsedValueSTR += string.Format("{0}={1};", keyPair.Key, keyPair.Value);
        }
        return parsedValueSTR;
    }




    public static void FirstTimeCrafting()
    {
        EventOnCurrentLevel("FirstTimeCrafting");
    }

    public static void ReviveRVWatched()
    {
        EventOnCurrentLevel("ReviveRVWatched");
    }



    static void EventOnCurrentLevel(string eventName)
    {
        Dictionary<string, object> args = new Dictionary<string, object>();

        args.Add("Level", LevelDataController.currentFakeLevel);

        CustomSDKEvent(eventName, args);
    }



    public static void CheckCoinAtThisLevel()
    {
        CheckLevelAndCash("CoinAtLevel");
    }


    
    public static void BodyShopOpened()
    {
        CheckLevelAndCash("BodyShopOpened");
    }

    public static void WeaponShopOpened()
    {
        CheckLevelAndCash("WeaponShopOpened");
    }

    public static void WeaponCustomizeOpened()
    {
        CheckLevelAndCash("WeaponCustomizeOpened");
    }

    public static void CoinRVWatched()
    {
        CheckLevelAndCash("CoinMultiplierRV");
    }


    static void CheckLevelAndCash(string eventname)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();

        dictionary.Add("Level", LevelDataController.currentFakeLevel);
        dictionary.Add("CashBalance", Currency.Balance(CurrencyType.COIN));

        CustomSDKEvent(eventname, dictionary);
    }




    public static void WeaponPurchasedCoin(string weaponName)
    {
        WeaponPurchased("WeaponPurchasedCoin", weaponName);
    }

    public static void WeaponPurchasedRV(string weaponName)
    {
        WeaponPurchased("WeaponPurchasedRV", weaponName);
    }

    static void WeaponPurchased(string eventName, string weaponName)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();

        dictionary.Add("Level", LevelDataController.currentFakeLevel);
        dictionary.Add("weaponName", weaponName);

        CustomSDKEvent(eventName, dictionary);
    }



    public static void LimbPurchasedCoin(string limbName)
    {
        LimbPurchased("LimbPurchasedCoin", limbName);
    }

    public static void LimbPurchasedRV(string limbName)
    {
        LimbPurchased("LimbPurchasedRV", limbName);
    }

    static void LimbPurchased(string eventName, string limbName)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();

        dictionary.Add("Level", LevelDataController.currentFakeLevel);
        dictionary.Add("LimbName", limbName);

        CustomSDKEvent(eventName, dictionary);
    }



    public static void HealthUpgraded()
    {
        StatsUpgraded("HealthUpgraded", "HealthLevel", GameData.Instance.healthLevel.value);
    }

    public static void DamageUpgraded()
    {
        StatsUpgraded("DamageUpgraded", "DamageLevel", GameData.Instance.damageLevel.value);
    }

    public static void EarningUpgraded()
    {
        StatsUpgraded("EarningUpgraded", "EarningLevel", GameData.Instance.earningLevel.value);
    }

    static void StatsUpgraded(string eventName, string statName, int statLevel)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();

        dictionary.Add("Level", LevelDataController.currentFakeLevel);
        dictionary.Add(statName, statLevel);

        CustomSDKEvent(eventName, dictionary);
    }
}
