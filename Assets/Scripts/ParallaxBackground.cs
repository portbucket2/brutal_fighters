using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    [SerializeField] private Vector2 parralax_multiplier;

    private Transform cam_transform;
    private Vector3 last_cam_pos;

    private void Start()
    {
        cam_transform = Camera.main.transform;
        last_cam_pos = cam_transform.position;
    }

    private void LateUpdate()
    {
        Vector3 delta_movement = cam_transform.position - last_cam_pos;

        transform.position += new Vector3(delta_movement.x * parralax_multiplier.x, delta_movement.y * parralax_multiplier.y);

        last_cam_pos = cam_transform.position;

    }

}
