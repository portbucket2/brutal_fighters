using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D.IK;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public enum TouchSide
    {
        right,
        left
    }
    public List<GameObject> show_on_fight_started;

    public float base_health_multiplier;

    private TouchSide touch_side = TouchSide.right;

    public float movement_speed;

    public Animator avatar_ik_anim;

    private Rigidbody2D player_rb;

    public float joy_max_distance;
    private Vector2 joystick_right_rest_pos;
    public GameObject joystick_right_base;
    public GameObject joystick_right_handle;
    private Vector2 touch_diff_right;

    private Vector2 joystick_left_rest_pos;
    public GameObject joystick_left_base;
    public GameObject joystick_left_handle;
    private Vector2 touch_diff_left;

    public float joy_diff_multiplier;
    public float joy_diff_multiplier_head;
    public GameObject shoulder_back;
    public GameObject shoulder_front;

    public GameObject forearm_back_target;
    private Vector2 forearm_back_rest;
    public GameObject head_target;
    private Vector2 head_rest;
    public GameObject forearm_front_target;
    private Vector2 forearm_front_rest;

    public float reset_speed;


    public List<RotationFollower> rotationFollow;

    public List<PositionFollower> positionFollow;

    public Rigidbody2D root;


    public Vector2 intended_position = new Vector2(-2, 0);

    //private Vector2 head_movement_offset = Vector2.zero;

    private float double_tap_right_timer = 0f;
    private float double_tap_left_timer = 0f;

    private int right_touch_id = -1;
    private int left_touch_id = -1;

    public Text[] debug_text;

    private HealthManager health_manager;
    //private Enemy enemy;
    public Rigidbody2D enemy_spine;

    private GameManager game_manager;

    private Tutorial_Fight tutorial;

    bool tapped_right = false; //Editor only

    [SerializeField]
    private float min_enemy_distance;

    private void Awake()
    {
        if (LevelDataController.currentFakeLevel == 1)
        {
            //transform.position = new Vector3(-5, 0, 0);
        }

        game_manager = FindObjectOfType<GameManager>();

        if (game_manager != null)
        {
            game_manager.OnStartFightEvent += ShowOnStartFight;
        }
    }

    private void Start()
    {
        tutorial = FindObjectOfType<Tutorial_Fight>();
        //enemy = FindObjectOfType<Enemy>();

        health_manager = GetComponent<HealthManager>();

        health_manager.max_health =  GameData.Instance.Health * base_health_multiplier;

        health_manager.health_bar.max_health = health_manager.max_health;

        health_manager.health_bar.Initiate();

        intended_position = transform.position;

        player_rb = GetComponent<Rigidbody2D>();

        Vector3 offset = Vector3.zero;
        offset.x = transform.position.x;
        
        forearm_front_rest = forearm_front_target.transform.position - offset;
        forearm_back_rest = forearm_back_target.transform.position - offset;
        head_rest = head_target.transform.position - offset;

        joystick_right_rest_pos = joystick_right_base.transform.position;
        joystick_left_rest_pos = joystick_left_base.transform.position;

        joy_max_distance = Screen.height * joy_max_distance / 990f;

    }


    private void Update()
    {
        if (!game_manager.fight_started)
        {
            return;
        }
        //debug_text[0].text = ((int)(1f / Time.unscaledDeltaTime)).ToString();

        if (double_tap_left_timer > 0)
        {
            double_tap_left_timer -= Time.deltaTime;
        }

        if (double_tap_right_timer > 0)
        {
            double_tap_right_timer -= Time.deltaTime;
        }

#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0)) // Tapping
        {
            if (Input.mousePosition.x > Screen.width / 2)
            {
                touch_side = TouchSide.right;

                Tapping(touch_side, Input.mousePosition);
            }
            else
            {
                touch_side = TouchSide.left;

                Tapping(touch_side, Input.mousePosition);
            }
        }

        if (Input.GetMouseButtonUp(0)) // Releasing
        {
            Released(touch_side);
        }

        if (Input.GetMouseButton(0)) // Dragging
        {
            Dragging(touch_side, Input.mousePosition);
        }
        else
        {
            Vector2 offset = Vector2.zero;
            offset.x = transform.position.x;

            forearm_back_target.transform.position = Vector2.Lerp(forearm_back_target.transform.position, forearm_back_rest + offset, Time.deltaTime * reset_speed);
            forearm_front_target.transform.position = Vector2.Lerp(forearm_front_target.transform.position, forearm_front_rest + offset, Time.deltaTime * reset_speed);
            head_target.transform.position = Vector2.Lerp(head_target.transform.position, head_rest + offset, Time.deltaTime * reset_speed);
        }
#else

        int touch_idx = 0;

        while (touch_idx < Input.touchCount)
        {

            Touch t = Input.GetTouch(touch_idx);

            //if (touch_idx < debug_text.Length)
            //{
            //    debug_text[touch_idx].text = t.position.ToString();
            //}

            if (t.phase == TouchPhase.Began)
            {
                if (t.position.x > Screen.width / 2)
                {
                    right_touch_id = t.fingerId;

                    Tapping(TouchSide.right, t.position);
                }
                else
                {
                    left_touch_id = t.fingerId;

                    Tapping(TouchSide.left, t.position);
                }
            }
            else if (t.phase == TouchPhase.Ended)
            {
                if (t.fingerId == right_touch_id)
                {
                    right_touch_id = -1;

                    Released(TouchSide.right);
                }

                if (t.fingerId == left_touch_id)
                {
                    left_touch_id = -1;

                    Released(TouchSide.left);
                }

            }
            else if (t.phase == TouchPhase.Moved)
            {
                if (t.fingerId == right_touch_id)
                {
                    Dragging(TouchSide.right, t.position);
                }


                if(t.fingerId == left_touch_id)
                {
                    Dragging(TouchSide.left, t.position);
                }
        }

            touch_idx++;
        }

        Vector2 offset = Vector2.zero;
        offset.x = transform.position.x;

        if(right_touch_id == -1)
        {
            forearm_front_target.transform.position = Vector2.Lerp(forearm_front_target.transform.position, forearm_front_rest + offset, Time.deltaTime * reset_speed);
        }
        if(left_touch_id == -1)
        {
            forearm_back_target.transform.position = Vector2.Lerp(forearm_back_target.transform.position, forearm_back_rest + offset, Time.deltaTime * reset_speed);
        }

        if(right_touch_id == -1 && left_touch_id == -1)
        {
            head_target.transform.position = Vector2.Lerp(head_target.transform.position, head_rest + offset, Time.deltaTime * reset_speed);
        }

#endif

        if (intended_position.x > game_manager.map_right_limit)
        {
            intended_position.x = game_manager.map_right_limit;
        }

        if (intended_position.x > enemy_spine.position.x - min_enemy_distance)
        {
            intended_position.x = enemy_spine.position.x - min_enemy_distance;
        }
        
        if (intended_position.x < game_manager.map_left_limit)
        {
            intended_position.x = game_manager.map_left_limit;
        }
    }

    private void FixedUpdate()
    {
        if (health_manager.is_dead)
        {
            return;
        }

        foreach (RotationFollower follower in rotationFollow)
        {
            follower.FollowRotation();
        }
        foreach (PositionFollower follower in positionFollow)
        {
            follower.FollowPosition();
        }

        player_rb.MovePosition(Vector2.Lerp(player_rb.position, intended_position, Time.deltaTime * movement_speed));
    }

    public void ShowOnStartFight()
    {
        if (LevelDataController.currentFakeLevel == 1)
        {
            return;
        }
        foreach (GameObject item in show_on_fight_started)
        {
            item.SetActive(true);
        }
    }

    private void MoveRight()
    {
        intended_position += Vector2.right * 2;
        avatar_ik_anim.Play("walk_forward", -1, 0);

        if (tutorial != null)
        {
            tutorial.DoubleTapDone();
        }
    }

    private void MoveLeft()
    {
        intended_position += Vector2.left * 2;
        avatar_ik_anim.Play("walk_backward", -1, 0);
    }


    private void Tapping(TouchSide touch_side, Vector2 tap_position)
    {
        if (touch_side == TouchSide.right)
        {
            joystick_right_base.transform.position = tap_position;

            if (double_tap_right_timer > 0)
            {
                MoveRight();
            }
            else
            {
                double_tap_right_timer = 0.5f;
            }
        }
        else
        {
            joystick_left_base.transform.position = tap_position;

            if (double_tap_left_timer > 0)
            {
                MoveLeft();
            }
            else
            {
                double_tap_left_timer = 0.5f;
            }
        }
    }

    private void Released(TouchSide touch_side)
    {
        if (touch_side == TouchSide.right)
        {
            touch_diff_right = Vector2.zero;
            joystick_right_handle.transform.localPosition = Vector2.zero;
            joystick_right_base.transform.position = joystick_right_rest_pos;
        }
        else
        {
            touch_diff_left = Vector2.zero;
            joystick_left_handle.transform.localPosition = Vector2.zero;
            joystick_left_base.transform.position = joystick_left_rest_pos;
        }
    }

    private void Dragging(TouchSide touch_side, Vector2 touch_position)
    {
        Vector2 joy_diff_right = joystick_right_handle.transform.position - joystick_right_base.transform.position;
        Vector2 joy_diff_left = joystick_left_handle.transform.position - joystick_left_base.transform.position;

        if (touch_side == TouchSide.right && joystick_right_base)
        {
            if (!joystick_right_base.activeInHierarchy)
            {
                return;
            }

            touch_diff_right = touch_position - (Vector2)joystick_right_base.transform.position;

            if (touch_diff_right.magnitude < joy_max_distance)
            {
                joystick_right_handle.transform.position = touch_position;
            }
            else
            {
                joystick_right_handle.transform.position = (Vector2)joystick_right_base.transform.position + touch_diff_right.normalized * joy_max_distance;
                joystick_right_base.transform.position = Vector2.Lerp(joystick_right_base.transform.position, touch_position, Time.deltaTime * 2f);
            }

            forearm_front_target.transform.position = (Vector2)shoulder_front.transform.position + joy_diff_right * joy_diff_multiplier + Vector2.right * 1f;
        }
        else
        {
            if (!joystick_left_base.activeInHierarchy)
            {
                return;
            }

            touch_diff_left = touch_position - (Vector2)joystick_left_base.transform.position;

            if (touch_diff_left.magnitude < joy_max_distance)
            {
                joystick_left_handle.transform.position = touch_position;
            }
            else
            {
                joystick_left_handle.transform.position = (Vector2)joystick_left_base.transform.position + touch_diff_left.normalized * joy_max_distance;
                joystick_left_base.transform.position = Vector2.Lerp(joystick_left_base.transform.position, touch_position, Time.deltaTime * 2f);
            }

            forearm_back_target.transform.position = (Vector2)shoulder_back.transform.position + joy_diff_left * joy_diff_multiplier + Vector2.right * 1f;
        }

        head_target.transform.position = (Vector2)transform.position + (joy_diff_right + joy_diff_left) / 1 * joy_diff_multiplier_head + Vector2.up * head_rest.y;
    }
}
