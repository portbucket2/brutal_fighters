using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodEnable : MonoBehaviour
{
    ParticleSystem blood;
    Color defaultColor;
    private void Awake()
    {
        blood = FindObjectOfType<ParticleSystem>();
        defaultColor = blood.startColor;

        Global.ShowBloodAction += ShowBlood;
    }

    private void OnEnable()
    {
        blood.gameObject.SetActive(Tweaker.show_blood);
    }

    private void OnDestroy()
    {
        Global.ShowBloodAction -= ShowBlood;
    }

    private void ShowBlood(bool value)
    {
        if (value)
        {
            blood.startColor = defaultColor;
        }
        else
        {
            blood.startColor = new Color(0, 0, 0, 0);
        }
    }
}
