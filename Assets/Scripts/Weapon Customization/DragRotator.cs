using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragRotator : MonoBehaviour
{
    public Transform pivot_center;
    private bool rotation_enabled = false;
    private Vector2 intended_position = Vector2.zero;
    private float distance;
    private Vector3 pos_offset = Vector2.zero;
    private WeaponPart weaponPart;


    private Vector3 touch_offset = Vector3.zero;

    private float touch_z_coord;

    Camera cam;

    public event Action onRotationUpdated;

    private void Awake()
    {
        cam = Camera.main;
        distance = (transform.position - pivot_center.position).magnitude;

        pos_offset = transform.position - pivot_center.position;

        weaponPart = pivot_center.GetComponent<WeaponPart>();
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 touchpos = Input.mousePosition;

        touchpos.z = touch_z_coord;

        return cam.ScreenToWorldPoint(touchpos);
    }

    private void OnMouseDown()
    {
        touch_z_coord = cam.WorldToScreenPoint(transform.position).z;

        touch_offset = transform.position - GetMouseWorldPos() - pivot_center.position;

        rotation_enabled = true;
        //transform.SetParent(null);
    }

    private void OnMouseUp()
    {
        rotation_enabled = false;

        //transform.SetParent(pivot_center);
        onRotationUpdated?.Invoke();
    }

    private void OnMouseDrag()
    {
        transform.position = (GetMouseWorldPos() + touch_offset).normalized * distance + pivot_center.position;

        Vector2 pos_diff = (transform.position - pivot_center.position);

        //transform.eulerAngles = Vector3.forward * Mathf.Atan2(-pos_diff.x, pos_diff.y) * Mathf.Rad2Deg;
        if (weaponPart.weapon_attach_check.weapon_panel == WeaponPanel.left)
        {
            pivot_center.eulerAngles = Vector3.forward * Mathf.Atan2(pos_diff.x, -pos_diff.y) * Mathf.Rad2Deg;
        }
        else
        {
            pivot_center.eulerAngles = Vector3.forward * Mathf.Atan2(-pos_diff.x, pos_diff.y) * Mathf.Rad2Deg;
        }

        //pivot_center.eulerAngles = transform.eulerAngles;

        pos_offset = transform.position - pivot_center.position;

    }

}
