using UnityEngine;
using UnityEngine.Events;

public class ExecuteAfterDelay : MonoBehaviour
{
    public float delay;
    public UnityEvent executeAfterDelay;

    public void Execute()
    {
        Invoke("DelayedExecute", delay);
    }

    void DelayedExecute()
    {
        executeAfterDelay?.Invoke();
    }
}
