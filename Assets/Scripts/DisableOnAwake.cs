using UnityEngine;

public class DisableOnAwake : MonoBehaviour
{
    public bool set_active = false;
    private void Awake()
    {
        gameObject.SetActive(set_active);
    }

}
