using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using PotatoSDK;
using GameAnalyticsSDK;
using ByteBrewSDK;
using LionStudios.Suite.Analytics;
using FRIA;

public class Intro : MonoBehaviour
{
    public bool resetLevels;

    public bool testing;
    public int startingMoney;
    public int testLevel = 1;

    public ShopItemKeeper shopItemKeeper;


    private void Awake()
    {
    }

    private void Start()
    {
        shopItemKeeper.EnsureInit();

        WeaponData.Instance.LoadWeaponData();

        StartCoroutine(Initiate());
        //GDPRMan.RequestGDPRFlow();
        //GDPRMan.onGDPR_FlowEnableDisable += GDPRClicked;
    }

    private void OnDestroy()
    {
        //GDPRMan.onGDPR_FlowEnableDisable -= GDPRClicked;
    }

    private void GDPRClicked()
    {
        Debug.LogError(GDPRMan.ConsentApplies);

        //StartCoroutine(Initiate());
    }

    private IEnumerator Initiate()
    {
        while (!Potato.IsReady)
        {
            yield return null;
        }

        //GameAnalytics.Initialize();
        LionAnalytics.GameStart();
        ByteBrew.InitializeByteBrew();

        if (resetLevels)
        {
            LevelDataController.currentFakeLevel = 1;
        }
        else if (testing)
        {
            Currency.Transaction(CurrencyType.COIN, startingMoney);
            LevelDataController.currentFakeLevel = testLevel;
        }

        if (LevelDataController.currentFakeLevel == 1)
        {
            WeaponData.Instance.LoadInitialWeaponSet();

            SceneManager.LoadScene(LevelDataController.CalculateNextLevel(0));
        }
        else
        {
            if (WeaponData.HasNoWeapon)
            {
                //SceneManager.LoadScene(1);
                SceneManager.LoadScene(LevelDataController.CalculateNextLevel(0));
            }
            else
            {
                SceneManager.LoadScene(LevelDataController.CalculateNextLevel(0));
            }
        }

    }

}
