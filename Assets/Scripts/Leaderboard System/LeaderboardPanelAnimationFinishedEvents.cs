using UnityEngine;
using UnityEngine.Events;

public class LeaderboardPanelAnimationFinishedEvents : MonoBehaviour
{
    public UnityEvent customEvent;

    public void CustomEventFire()
    {
        customEvent?.Invoke();
    }
}
