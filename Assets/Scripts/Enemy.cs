using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public DIFFICULTY currentDifficulty;
    [SerializeField]
    private float walk_step_size = 2;

    public float player_min_dist;
    public Animator animator;
    public float movement_speed;

    public float rand_action_dur_min;
    public float rand_action_dur_max;

    private HealthManager health_manager;

    public float reset_speed;

    public List<RotationFollower> rotationFollow;
    public List<PositionFollower> positionFollow;

    public Rigidbody2D spine;

    private Rigidbody2D rb;
    private Vector2 intended_position = Vector2.zero;


    private GameManager game_manager;

    private bool is_ragdoll = false;
    private float temp_ragdoll_cooldown = 0f;

    private EnemyProgression enemy_progression;

    private bool acting_started = false;
    int attackAnimationMin;
    int attackAnimationMax;

    private int idx_for_level = 0;
    [SerializeField] EnemyAI aI_Data;
    [Header("Debug : ")]
    public Transform player_transform;
    [SerializeField] HealthManager playerHealth;
    [SerializeField] HealthXDifficulty healthXDifficulty;

    private void Start()
    {
        game_manager = FindObjectOfType<GameManager>();
        intended_position = transform.position;
        rb = GetComponent<Rigidbody2D>();
        //player = FindObjectOfType<PlayerController>();

        idx_for_level = LevelDataController.CalculateLevelRotation() - 1;

        enemy_progression = GetComponent<EnemyProgression>();

        health_manager = GetComponent<HealthManager>();

        health_manager.health_bar.max_health = health_manager.max_health * enemy_progression.base_health_multiplier;
        health_manager.health_bar.Initiate();

        if (LevelDataController.currentFakeLevel > 1)
        {
            game_manager.OnStartFightEvent += StartActing;
        }
        else
        {
            Tutorial_Fight tut_fight = FindObjectOfType<Tutorial_Fight>();

            if (tut_fight)
            {
                tut_fight.OnDragTutDoneEvent += StartActing;
            }
        }
        if (player_transform)
        {
            playerHealth = player_transform.GetComponent<HealthManager>();
        }

        List<HealthXDifficulty> hxd = new List<HealthXDifficulty>();
        hxd = aI_Data.GetList();
        if (idx_for_level < hxd.Count)
        {
            healthXDifficulty = hxd[idx_for_level];
        }
        else
        {
            healthXDifficulty = hxd[hxd.Count - 1];
        }
        DetermineDifficulty();
    }

    void StartActing()
    {
        if (acting_started)
        {
            return;
        }

        acting_started = true;

        Invoke("RandomAction", Random.Range(rand_action_dur_min, rand_action_dur_max));
    }
    void SetWeaponDamage(int damage)
    {
        List<LightSaber> weapons = enemy_progression.weapon_root;
        foreach (LightSaber weapon in weapons)
        {
            weapon.attack_damage = damage;
        }
    }
    public void SetDifficulty(DIFFICULTY diff)
    {
        currentDifficulty = diff;
        switch (currentDifficulty)
        {
            case DIFFICULTY.EASY:
                EasyDifficulty();
                break;
            case DIFFICULTY.MIDIUM:
                MidDifficulty();
                break;
            case DIFFICULTY.HARD:
                HardDifficulty();
                break;
            default:
                break;
        }
    }
    public virtual void Attack()
    {
        //animator.Play("attack_1", -1, 0);
        int cc = Random.Range(attackAnimationMin, attackAnimationMax);
        string ss = "attack_" + cc;
        //animator.Play("attack_1", -1, 0);
        animator.Play(ss, -1, 0);
    }

    public void MoveRight(float step_size)
    {
        intended_position += Vector2.right * step_size;
        animator.Play("walk_backward", -1, 0);
    }

    public void MoveLeft(float step_size)
    {
        intended_position += Vector2.left * step_size;
        animator.Play("walk_forward", -1, 0);
    }

    public void RandomAction()
    {
        DetermineDifficulty();
        if (health_manager.can_execute_finisher)
        {
            animator.enabled = false;
            return;
        }

        if (Mathf.Abs(transform.position.x - player_transform.position.x) > 4)
        {
            MoveLeft(walk_step_size);
            Invoke("RandomAction", 0.5f);
        }
        else if (Mathf.Abs(transform.position.x - player_transform.position.x) < player_min_dist)
        {
            MoveRight(walk_step_size);
            Invoke("RandomAction", 0.5f);
        }
        else
        {
            int action = Random.Range(0, 10);

            if (action == 0)
            {
                if (player_transform.position.x < transform.position.x - 1)
                {
                    MoveRight(walk_step_size);
                    //Attack();
                }
                else
                {
                    MoveLeft(walk_step_size);
                }
            }
            else if (action == 1)
            {
                if (transform.position.x < game_manager.map_right_limit)
                {
                    MoveRight(walk_step_size);
                    //Attack();
                }
                else
                {
                    MoveLeft(walk_step_size);
                }
            }
            else if (action > 2)
            {
                Attack();
            }
            AnimatorClipInfo[] currentClipInfo = animator.GetCurrentAnimatorClipInfo(0);
            //Access the current length of the clip

            float clipLength = 1;
            try
            {
                if (currentClipInfo[0].clip)
                    clipLength = currentClipInfo[0].clip.length;
                else
                    clipLength = 1f;
            }
            catch (System.Exception)
            {
                throw;
            }

            //Access the Animation clip name
            string clipName = currentClipInfo[0].clip.name;
            Debug.Log(clipName + " length : "+ clipLength);
            Invoke("RandomAction", clipLength + Random.Range(rand_action_dur_min, rand_action_dur_max));

        }

    }

    private void FixedUpdate()
    {
        if (health_manager.is_dead)
        {
            return;
        }

        if (temp_ragdoll_cooldown > 0)
        {
            Vector2 target_position = rb.position;
            target_position.x = spine.position.x;
            rb.position = target_position;
            intended_position = rb.position;

            temp_ragdoll_cooldown -= Time.fixedDeltaTime;
            return;
        }

        foreach (RotationFollower follower in rotationFollow)
        {
            follower.FollowRotation();
        }

        foreach (PositionFollower follower in positionFollow)
        {
            follower.FollowPosition();
        }

        rb.MovePosition(Vector2.Lerp(rb.position, intended_position, Time.deltaTime * movement_speed));
    }


    public void TemporaryRagdoll(float duration)
    {
        //is_ragdoll = true;

        //Invoke("TemporaryRagdollOff", duration);

        temp_ragdoll_cooldown = duration;
    }

    private void TemporaryRagdollOff()
    {
        is_ragdoll = false;
    }

    public void FinisherInvoked()
    {
        MoveRight(walk_step_size * 3);
    }

    void EasyDifficulty()
    {
        SetWeaponDamage(aI_Data.easy_attackDamage);
        rand_action_dur_min = aI_Data.easy_nextAttackGapMin;
        rand_action_dur_max = aI_Data.easy_nextAttackGapMax;

        attackAnimationMin = aI_Data.easy_attackAnimationMin;
        attackAnimationMax = aI_Data.easy_attackAnimationMax;
    }
    void MidDifficulty()
    {
        SetWeaponDamage(aI_Data.mid_attackDamage);
        rand_action_dur_min = aI_Data.mid_nextAttackGapMin;
        rand_action_dur_max = aI_Data.mid_nextAttackGapMax;

        attackAnimationMin = aI_Data.mid_attackAnimationMin;
        attackAnimationMax = aI_Data.mid_attackAnimationMax;
    }
    void HardDifficulty()
    {
        SetWeaponDamage(aI_Data.hard_attackDamage);
        rand_action_dur_min = aI_Data.hard_nextAttackGapMin;
        rand_action_dur_max = aI_Data.hard_nextAttackGapMax;

        attackAnimationMin = aI_Data.hard_attackAnimationMin;
        attackAnimationMax = aI_Data.hard_attackAnimationMax;
    }
    void DetermineDifficulty()
    {
        float health = playerHealth.GetHealthPercentage();
        if (health > 0.70)
        {
            SetDifficulty(healthXDifficulty.diffAt100);
        }
        else if (health < 0.70 && health > 0.50)
        {
            SetDifficulty(healthXDifficulty.diffAt70);
        }
        else if (health < 0.50 && health > 0.20)
        {
            SetDifficulty(healthXDifficulty.diffAt50);
        }
        else if (health < 0.20)
        {
            SetDifficulty(healthXDifficulty.diffAt20);
        }
        else
        {
            SetDifficulty(healthXDifficulty.diffAt20);
        }
        Debug.Log("player Health : " + health + " difficulty : " + currentDifficulty.ToString());
    }
}
