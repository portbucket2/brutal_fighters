using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LeaderboardPanel : MonoBehaviour
{
    [SerializeField] bool isPlayer = false;

    [SerializeField] Sprite bronzeMedal;
    [SerializeField] Sprite silverMedal;
    [SerializeField] Sprite goldMedal;
    
    [Space]
    [SerializeField] TMP_Text rankText;
    [SerializeField] TMP_Text nameText;
    [SerializeField] TMP_Text scoreText;

    [Space]
    [SerializeField] Image medal;

    public void Init(int rank, int score)
    {
        if (rank < 3000)
        {
            medal.sprite = goldMedal;
        }
        else if (rank < 6000)
        {
            medal.sprite = silverMedal;
        }
        else
        {
            medal.sprite = bronzeMedal;
        }

        rankText.text = "#" + rank.ToString();
        scoreText.text = score.ToString();
    }

    public void SetName(string personName)
    {
        if (isPlayer)
        {
            return;
        }
        nameText.text = personName;
    }
}
