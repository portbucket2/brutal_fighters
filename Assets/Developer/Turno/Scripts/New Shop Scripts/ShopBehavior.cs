using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShopBehavior : MonoBehaviour
{
    [Header("Item Section")]
    public ShopItemKeeper itemCollection;    
    public GameObject itemPrefab;

    [Header("Tab Section")]
    public TabsUIHorizontal tabsUI;
    public List<Transform> TabContent = new List<Transform>();
    [SerializeField]
    protected int currentTabIndex;

    protected virtual void Awake()
    {
        itemCollection.EnsureInit();
    }
    protected virtual void Start()
    {
        ContentPreview.itemSelectedListener += OnItemSelection;
        ContentPreview.itemPurchasedListener += OnItemPurchase;
        tabsUI.OnTabChange.AddListener(OnTabChanged);
        currentTabIndex = 0;
        PopulateTabContent();
    }

    public virtual void OnDestroy()
    {
        ContentPreview.itemSelectedListener -= OnItemSelection;
        ContentPreview.itemPurchasedListener -= OnItemPurchase;
        tabsUI.OnTabChange.RemoveListener(OnTabChanged);

    }
    protected virtual void OnTabChanged(int index)
    {
        currentTabIndex = index;
        CheckCostAvailability();
    }
    public abstract void OnItemSelection(ShopItemSO item, ContentPreview contentPreview);
    public abstract void OnItemPurchase(ShopItemSO itemData, int itemId);

    public virtual void OnEquip()
    {

    }
    public abstract void PopulateTabContent();
    protected void CheckCostAvailability()
    {
        //return;
        for (int i = 0; i < TabContent[currentTabIndex].childCount; i++)
        {
            TabContent[currentTabIndex].GetChild(i)
           .GetComponent<ContentPreview>().CostTextChange();
        }
    }
}
