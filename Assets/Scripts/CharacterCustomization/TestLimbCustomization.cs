using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLimbCustomization : MonoBehaviour
{
    public CustomLimbsManager custom_limbs_manager;

    private int current_top = 0;

    public void TopPlus()
    {
        current_top++;
        custom_limbs_manager.TestSetUpperBodyLimbSize(current_top);
    }

    public void TopMinus()
    {
        current_top--;
        custom_limbs_manager.TestSetUpperBodyLimbSize(current_top);
    }

    public void BottomPlus()
    {

    }

    public void BottomMinus()
    {

    }
}
