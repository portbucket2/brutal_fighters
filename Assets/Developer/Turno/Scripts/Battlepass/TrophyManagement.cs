using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TrophyManagement : BaseTrophyManager
{
    private float _valueToLerp = 0;
    private float _current = 0;
    private float _target = 1;
    private int temp = 0;
    private int currentTrophyLevel = 0;
    private int totalBlock;
    private float blockWidth;
    private float sliderWidth;
    private float targetValue;
    private float currentValue;
    private RectTransform contentRect;
    Coroutine runningSliderRoutine;
    private bool shouldExecute;

    private void Awake()
    {
        Populate();
        UpdateSlider();
        shouldExecute = false ;
        collection.EnsureInit();
        contentRect = contentHolder.parent.GetComponent<RectTransform>();
        contentTargetPosition = Vector2.zero;
        
    }
    IEnumerator Start()
    {
        yield return null;
        SetContentUpperLimit();
        scrollRect.enabled = false;
        float lerpValue = slider.value / slider.maxValue;
        contentTargetPosition = Vector2.Lerp(contentLowerLimit, contentUpperLimit, lerpValue);
        contentRect.localPosition = contentTargetPosition;
        nextUnlockable.text = $"Reward {collection.GetAchievementTargetID + 1}";

    }
    private void OnEnable()
    {
        if (runningSliderRoutine != null)
        {
            StopCoroutine(runningSliderRoutine);
            runningSliderRoutine = null;
        }
        Currency.coinMan.AddListner_BalanceChanged(CurrencyType.XP, OnTrophyChange);
        slider.onValueChanged.AddListener(SliderValueChanged);
        RewardPreview.onSelectionAction += OnSelection;
        RewardPreview.trophyAchievedAction += TrophyAchieved;
        nextButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(LevelDataController.CalculateNextLevel(1));
        });
    }

   

    private void OnDisable()
    {
        Currency.coinMan.RemoveListner_BalanceChanged(CurrencyType.XP, OnTrophyChange);
        slider.onValueChanged.RemoveListener(SliderValueChanged);
        RewardPreview.onSelectionAction -= OnSelection;
        RewardPreview.trophyAchievedAction -= TrophyAchieved;
        nextButton.onClick.RemoveAllListeners();
    }
    private void OnSelection(Sprite sprite)
    {
        itemPreview.sprite = sprite;
    }
    private void TrophyAchieved(int arg0, Sprite icon)
    {
        itemPreview.sprite = icon;
        if (arg0 < collection.unlockedByTrophyCollection.Count - 1)
        {
            nextUnlockable.text = $"Reward {arg0 + 2}";
        }
        
    }
    public override void Populate()
    {
        temp = 1;
        foreach (var item in collection.unlockedByTrophyCollection.Select((value, i) => (value, i)))
        {
            int blankFactor = item.value.GetUnlockAtLevel - temp;
            for (int i = 0; i < blankFactor; i++)
            {
                Instantiate(emptyRewardPrefab, contentHolder);
            }
            GameObject reward = Instantiate(rewardPrefab, contentHolder);
            RewardPreview preview = reward.GetComponent<RewardPreview>();
            preview.LoadContent(item.value, item.i);
            temp = item.value.GetUnlockAtLevel + 1;
        }
        for (int i = 0; i < collection.unlockedByTrophyCollection.Count; i++)
        {

        }
    }

    private void OnTrophyChange()
    {
        currentTrophyLevel++;
        targetValue = ((GetTrophyLevel() * blockWidth) + blockWidth / 2) / sliderWidth;
        if (runningSliderRoutine != null) StopCoroutine(runningSliderRoutine);
        runningSliderRoutine = StartCoroutine(SliderRoutine());
        //requiredTrophyForNext.text = collection.trophyUnlockableCollection[ShopItemKeeper.Instance.nextTrophyUnlockable].GetTrophyRequiredToUnlock.ToString();
    }
    public override void UpdateScrollPosition()
    {
        
    }
    private void SliderValueChanged(float value)
    {
        if (shouldExecute)
        {
            float lerpValue = value / slider.maxValue;
            contentTargetPosition = Vector2.Lerp(contentLowerLimit, contentUpperLimit, lerpValue);
            Vector2 startLocalPosition = contentRect.localPosition;
            contentRect.localPosition = Vector2.Lerp(startLocalPosition, contentTargetPosition, Time.deltaTime * contentAnimationSpeed);
        }
        shouldExecute = true;

    }
    private int GetTrophyLevel()
    {
        return Mathf.RoundToInt(Currency.Balance(CurrencyType.XP) /
                                ConstantManager.perLevelTrophyEarning);
    }
    public override void UpdateSlider()
    {
        totalBlock = contentHolder.childCount;
        blockWidth = rewardPrefab.GetComponent<RectTransform>().rect.size.x;
        RectTransform sliderRect = slider.GetComponent<RectTransform>();
        sliderWidth = blockWidth * totalBlock - blockWidth / 2;
        sliderRect.sizeDelta = new Vector2( sliderWidth,
                                            sliderRect.sizeDelta.y);

        targetValue = ((GetTrophyLevel() * blockWidth) + blockWidth / 2) / sliderWidth;
        slider.value = targetValue;

        
    }
    private void SetContentUpperLimit()
    {    
        float wDelta = contentRect.sizeDelta.x;
        contentUpperLimit.x = -wDelta;
    }
    IEnumerator SliderRoutine()
    {
        float timeElapsed = 0;
        currentValue = slider.value;
        while (timeElapsed < lerpDuration)
        {
            //_valueToLerp = Mathf.MoveTowards(_current, _target, timeElapsed / lerpDuration);
            slider.value = Mathf.Lerp(currentValue, targetValue , EaseOut(timeElapsed / lerpDuration));
            
            
            yield return null;

            timeElapsed += Time.deltaTime;
        }
        //_valueToLerp = _target;
        slider.value = targetValue;
        scrollRect.enabled = true;
    }
    private static float EaseOut(float t)
    {
        return Mathf.Sqrt(1 - Mathf.Pow(1 - t, 2));
    }
}
