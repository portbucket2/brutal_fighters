using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class PlayerUpgrader : MonoBehaviour
{
    public WalletObserver walletObserver;

    [SerializeField] Color canBuyColor;
    [SerializeField] Color cannotBuyColor;
    [SerializeField] UpgradeType upgradeType;
    private enum UpgradeDataType
    {
        currentValue,
        upgradeCost,
    }


    [SerializeField] TMP_Text currentValue;
    [SerializeField] TMP_Text upgradeCost;

    private void Start()
    {

        UpdateTexts();

        walletObserver.WalletValueChanged += UpdateTexts;
    }


    public void UpdateTexts()
    {
        ButtonStatus currentValueStatus;
        ButtonStatus upgradeCostStatus;

        currentValueStatus = GetValueOfType(upgradeType, UpgradeDataType.currentValue);
        upgradeCostStatus = GetValueOfType(upgradeType, UpgradeDataType.upgradeCost);

        currentValue.text = currentValueStatus.text;
        upgradeCost.text = upgradeCostStatus.text;

        upgradeCost.color = upgradeCostStatus.color;

    }

    public void UpgradeButtonPressed()
    {
        int coinBalance = Currency.Balance(CurrencyType.COIN);
        switch (upgradeType)
        {
            case UpgradeType.HEALTH:

                if (coinBalance < GameData.Instance.HealthUpgradeCost)
                {
                    Global.NotEnoughCoinAction?.Invoke();
                    return;
                }

                CustomEventTracker.HealthUpgraded();

                break;
            case UpgradeType.DAMAGE:

                if (coinBalance < GameData.Instance.DamageUpgradeCost)
                {
                    Global.NotEnoughCoinAction?.Invoke();
                    return;
                }

                CustomEventTracker.DamageUpgraded();

                break;
            case UpgradeType.COIN:

                if (coinBalance < GameData.Instance.EarningUpgradeCost)
                {
                    Global.NotEnoughCoinAction?.Invoke();
                    return;
                }

                CustomEventTracker.EarningUpgraded();

                break;
            default:
                break;
        }
        GameData.Instance.UpgradeAttributeNow(upgradeType);
        walletObserver.WalletValueChanged?.Invoke();
        Global.CoinMinusAction?.Invoke(transform.position);
    }

    private ButtonStatus GetValueOfType(UpgradeType type, UpgradeDataType dataType)
    {
        ButtonStatus buttonStatus = new ButtonStatus();

        if (dataType == UpgradeDataType.currentValue)
        {
            switch (type)
            {
                case UpgradeType.HEALTH:
                    buttonStatus.text = GameData.Instance.Health.ToString();

                    break;
                case UpgradeType.DAMAGE:
                    buttonStatus.text = GameData.Instance.Damage.ToString();

                    break;

                case UpgradeType.COIN:
                    buttonStatus.text = "x " + GameData.Instance.EarningMultiplier.ToString("F1");

                    break;

                default:
                    break;
            }
            
        }
        else if (dataType == UpgradeDataType.upgradeCost)
        {
            int cost = 0;

            switch (type)
            {
                case UpgradeType.HEALTH:
                    cost = GameData.Instance.HealthUpgradeCost;

                    buttonStatus.text = cost.ToString();
                    break;

                case UpgradeType.DAMAGE:
                    cost = GameData.Instance.DamageUpgradeCost;

                    buttonStatus.text = cost.ToString();
                    break;

                case UpgradeType.COIN:
                    cost = GameData.Instance.EarningUpgradeCost;

                    buttonStatus.text = cost.ToString();
                    break;

                default:
                    break;
            }

            if (cost > Currency.Balance(CurrencyType.COIN))
            {
                buttonStatus.color = cannotBuyColor;
            }
            else
            {
                buttonStatus.color = canBuyColor;
            }
        }



        return buttonStatus;
    }


    private class ButtonStatus
    {
        public string text;
        public Color color;

        public ButtonStatus()
        {
            text = "";
        }
    }

}
