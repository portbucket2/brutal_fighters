using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Text = TMPro.TextMeshProUGUI;
using System;
using PotatoSDK;
using FRIA;

public class ContentPreview : MonoBehaviour
{
    public ShopItemSO itemData;
    public Image assetPreview;
    public Text price;
    public GameObject PriceHolder;
    public GameObject equppiedTick;
    public GameObject selectedHighlight;
    [Header("ButtonGroup Section")]
    public GameObject unlockedWithAdGroup;
    public Button unlockUsingAdButton;
    public GameObject unlockedWithCoinGroup;
    public Button unlockUsingCoinButton;
    public GameObject unlockedWithTrophy;
    public Text trophyText;
    public static UnityAction<ShopItemSO, ContentPreview> itemSelectedListener;
    public static UnityAction<ShopItemSO, int> itemPurchasedListener;
    public static UnityAction<ShopItemSO> itemSelectedAction;
    public static UnityAction<ShopItemSO> itemPurchasedAction;
    public static UnityAction<ShopItemSO> weaponPurchasedAction;

    public static Action itemPurchased_Johnny_Potti;

    WeaponPickButtonUI weaponPickButtonUI;

    private void Start()
    {
        //lockedMask?.SetActive(!weaponData.PurchaseStatusProperty);

        //CostTextChange();

        if (!itemData.isSelectable)
        {
            weaponPickButtonUI = gameObject.AddComponent<WeaponPickButtonUI>();

            weaponPickButtonUI.id = itemData.GetId;
            weaponPickButtonUI.locked = itemData.GetUnlockMethod != ShopItemSO.ItemUnlockMethod.Free && !itemData.IsPurchased;
            weaponPickButtonUI.defaultPanel = WeaponPanel.right;

            //weaponPickButtonUI.Init();
        }

    }
    private void OnEnable()
    {
        Currency.coinMan.AddListner_BalanceChanged(CurrencyType.COIN, CostTextChange);
        unlockUsingCoinButton.onClick.AddListener(Purchase);
        unlockUsingAdButton.onClick.AddListener(PurchaseByAd);

        itemPurchased_Johnny_Potti += CostTextChange;

        weaponPurchasedAction += HideButtonAfterPurchase;

    }

   
    private void OnDisable()
    {
        Currency.coinMan.RemoveListner_BalanceChanged(CurrencyType.COIN, CostTextChange);
        unlockUsingCoinButton.onClick.RemoveListener(Purchase);

        itemPurchased_Johnny_Potti -= CostTextChange;
        weaponPurchasedAction -= HideButtonAfterPurchase;
    }
    public void CostTextChange()
    {
        //$"{Currency.Balance(CurrencyType.COIN)} is current cash".Debug("00FF00");
        price.color = Currency.Balance(CurrencyType.COIN) >= itemData.GetCost ? Color.white : Color.red;
    }

    public void SetAssetPreviewSprite(Sprite sprite)
    {
        assetPreview.sprite = sprite;
    }

    public void OnSelection()
    {
        itemSelectedListener?.Invoke(itemData, this);
        itemSelectedAction?.Invoke(itemData);
    }
    public void SetPurchaseStatus(bool isUnlocked)
    {
        PriceHolder.SetActive(!isUnlocked);
        //equppiedTick?.SetActive(isUnlocked);
    }
    public void EquippedTick(bool isEquipped)
    {
        equppiedTick?.SetActive(isEquipped);
    }
    public void SetSelectionHighlightStatus(bool isActive)
    {
        selectedHighlight.SetActive(isActive);
    }

    public void HideButtonAfterPurchase(ShopItemSO data)
    {
        if (itemData.GetId == data.GetId)
        {
            unlockedWithCoinGroup.SetActive(false);
            
            if (weaponPickButtonUI)
            {
                weaponPickButtonUI.locked = false;
            }
        }
    }

    public void Purchase()
    {
        $"{itemData.GetWeaponName} costs {itemData.GetCost}!".Debug("FF7373");
        if (Currency.Balance(CurrencyType.COIN) >= itemData.GetCost)
        {
            $"Purchase Button Pressed".Debug("C3C3C3");
            itemData.SetPurchased();
            Currency.Transaction(CurrencyType.COIN, -itemData.GetCost);
            if (itemData.isSelectable)
            {
                itemPurchasedListener?.Invoke(itemData, itemData.GetId);
                itemPurchasedAction?.Invoke(itemData);
                GameData.Instance.player_knows_limb_upgrade.value = 1;
            }
            else
            {
                weaponPickButtonUI.locked = false;
            }
            //Global.ItemUnlockedAction?.Invoke(itemData.GetIcon);
            weaponPurchasedAction?.Invoke(itemData);
            unlockedWithCoinGroup.SetActive(false);
            Global.CoinMinusAction?.Invoke(gameObject.transform.position);
        }
        else
        {
            Global.NotEnoughCoinAction?.Invoke();
            $"{itemData.GetCost} is not enough".Debug("C3C3C3");
        }
    }
    private void PurchaseByAd()
    {
        MAXMan.ShowRV(OnPurchaseRV);
    }

    private void OnPurchaseRV(bool isRvCompleted)
    {
        if (!isRvCompleted)
            return;
        else
        {
            itemData.SetPurchased();
            if (itemData.isSelectable)
            {
                itemPurchasedListener?.Invoke(itemData, itemData.GetId);
                itemPurchasedAction?.Invoke(itemData);
                GameData.Instance.player_knows_limb_upgrade.value = 1;
            }
            else
            {
                weaponPickButtonUI.locked = false;
            }
            weaponPurchasedAction?.Invoke(itemData);
            unlockedWithAdGroup.SetActive(false);
        }
    }

    public void LoadData(ShopItemSO item)
    {
        SetPurchaseStatus(item.IsPurchased);
        itemData = item;
        price.text = item.GetCost.ToString();
        price.color = Currency.Balance(CurrencyType.COIN) >= itemData.GetCost ? Color.white : Color.red;
        SetAssetPreviewSprite(item.GetIcon);
        switch (item.GetUnlockMethod)
        {
            case ShopItemSO.ItemUnlockMethod.Ads:
                unlockedWithAdGroup.SetActive(true);
                break;
            case ShopItemSO.ItemUnlockMethod.Coins:
                unlockedWithCoinGroup.SetActive(true);
                break;
            case ShopItemSO.ItemUnlockMethod.Free:
                break;
            case ShopItemSO.ItemUnlockMethod.Trophy:
                unlockedWithTrophy.SetActive(!item.IsPurchased);
                if (unlockedWithTrophy.activeInHierarchy)
                {
                    trophyText.text = $"unlocks at Level {item.GetUnlockAtLevel}";
                }
                break;
        }

    }
}
