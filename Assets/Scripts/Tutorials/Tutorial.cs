using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial : MonoBehaviour
{
    public int target_level = 1;

    public UnityEvent level_specific_event;

    public bool InvokeLevelSpecificEvents()
    {
        if (LevelDataController.currentFakeLevel == target_level)
        {
            level_specific_event?.Invoke();
            return true;
        }

        return false;
    }
}
