using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Random Name Generator", menuName = "Random Name Generator")]

public class RandomNameGenerator : ScriptableObject
{
    [SerializeField] List<string> names;

    private List<string> eliminatedNames;

    private void Awake()
    {
        eliminatedNames = new List<string>(names);

    }

    public string GetRandomName()
    {
        return names[Random.Range(0, names.Count)];
    }

    public string GetRandomNameOnce()
    {
        if (eliminatedNames.Count == 0)
        {
            eliminatedNames = names;
        }

        int idx = Random.Range(0, eliminatedNames.Count);

        string personName = eliminatedNames[idx];

        eliminatedNames.RemoveAt(idx);


        return personName;
}
}
