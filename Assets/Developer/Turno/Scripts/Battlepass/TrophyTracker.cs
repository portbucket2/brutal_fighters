using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using FRIA;

#if UNITY_EDITOR
using UnityEditor;
#endif  

public class TrophyTracker : MonoBehaviour
{
    public TextMeshProUGUI coinDisplayText;
    public TextMeshProUGUI TrophyTargetText;
    public TextMeshProUGUI nextTrophyID;
    //public Transform global;
    public GameObject tapHereNext;
    //public GameObject tapHerePopup;
    public CurrencyType currencyType;
    public float timeToAnimate = 1;
    public float stepTime = 1 / 20.0f;
    public ShopItemKeeper collection;
    public float delayedTarget = 1f;

    int savedValue;
    int displayValue;
    int targetDisplayValue;
    public bool pauseExternal;
    private int trophyTarget;
    
    void OnEnable()
    {
        savedValue = Currency.Balance(currencyType);
        
        displayValue = savedValue;
        if (runningTextRoutine != null)
        {
            StopCoroutine(runningTextRoutine);
            runningTextRoutine = null;
        }
        if (runningNextTextRoutine != null)
        {
            StopCoroutine(runningNextTextRoutine);
            runningNextTextRoutine = null;
        }
        Currency.coinMan.AddListner_BalanceChanged(currencyType, OnCoinChange);
        RewardPreview.trophyAchievedAction += AppendNextRequiredTrophy;
        Global.CloseButtonAction += PopupClosed;
        this.Add_DelayedAct(() =>
        {
            if (LevelDataController.currentFakeLevel < 2)
            {
                Global.Instance.itemTutorialTapHere.SetActive(true);
            }
            else
            {
                Global.Instance.itemTutorialTapHere.SetActive(false);
            }
        }, 2f);

    }

    private void PopupClosed()
    {
        this.Add_DelayedAct(() =>
        {
            if (LevelDataController.currentFakeLevel < 2)
            {
                Global.Instance.itemTutorialTapHere.SetActive(false);
                tapHereNext.SetActive(true);
            }
        }, 1.25f);
    }

    private void OnDisable()
    {
        Currency.coinMan.RemoveListner_BalanceChanged(currencyType, OnCoinChange);
        RewardPreview.trophyAchievedAction -= AppendNextRequiredTrophy;
        Global.CloseButtonAction -= PopupClosed;
        tapHereNext.SetActive(false);
        
    }
    private void Awake()
    {
        collection.EnsureInit();
    }
    private void Start()
    {
        trophyTarget = collection.unlockedByTrophyCollection[collection.GetAchievementTargetID].GetTrophyRequiredToUnlock;
        coinDisplayText.text = $"{savedValue}/";
        TrophyTargetText.text = $"{trophyTarget}";
        
    }
    void OnCoinChange()
    {
        savedValue = Currency.Balance(currencyType);
        if (runningTextRoutine != null) StopCoroutine(runningTextRoutine);
        runningTextRoutine = StartCoroutine(TextRoutine(savedValue));
    }
    private void AppendNextRequiredTrophy(int id, Sprite sprite)
    {
        targetDisplayValue = collection.unlockedByTrophyCollection[id].GetTrophyRequiredToUnlock;
        collection.IncrementTarget();
        trophyTarget = collection.unlockedByTrophyCollection[collection.GetAchievementTargetID].GetTrophyRequiredToUnlock;
        if (runningNextTextRoutine != null) StopCoroutine(runningNextTextRoutine);
        runningNextTextRoutine = StartCoroutine(NextTextRoutine(trophyTarget));
    }


    Coroutine runningTextRoutine;
    Coroutine runningNextTextRoutine;
    IEnumerator TextRoutine(int savedValue)
    {
        float startTime = Time.realtimeSinceStartup;
        int stepCount = Mathf.RoundToInt(timeToAnimate / this.stepTime);

        float stepTime = timeToAnimate / stepCount;
        int diff = savedValue - displayValue;
        int initialValue = displayValue;
        for (int i = 0; i < stepCount; i++)
        {
            while (pauseExternal)
            {
                yield return null;
                startTime += Time.deltaTime;
            }

            yield return new WaitForSeconds(stepTime);

            float p = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / timeToAnimate);
            displayValue = initialValue + Mathf.RoundToInt(diff * p);
            coinDisplayText.text = $"{displayValue}/";
        }
        displayValue = savedValue;
        coinDisplayText.text = $"{displayValue}/";
        //if (collection.itemIdForTrophy < collection.unlockedByTrophyCollection.Count)
        //{
        //    next = collection.unlockedByTrophyCollection[collection.itemIdForTrophy].GetTrophyRequiredToUnlock;         
        //}
    }
    IEnumerator NextTextRoutine(int savedValue)
    {
        yield return new WaitForSeconds(delayedTarget);
        float startTime = Time.realtimeSinceStartup;
        int stepCount = Mathf.RoundToInt(timeToAnimate / this.stepTime);

        float stepTime = timeToAnimate / stepCount;
        int diff = savedValue - targetDisplayValue;
        int initialValue = targetDisplayValue;
        for (int i = 0; i < stepCount; i++)
        {
            while (pauseExternal)
            {
                yield return null;
                startTime += Time.deltaTime;
            }

            yield return new WaitForSeconds(stepTime);

            float p = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / timeToAnimate);
            targetDisplayValue = initialValue + Mathf.RoundToInt(diff * p);
            TrophyTargetText.text = $"{targetDisplayValue}";
        }
        targetDisplayValue = savedValue;
        trophyTarget = savedValue;
        TrophyTargetText.text = $"{targetDisplayValue}";
        
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(TrophyTracker))]
public class TrophyTrackerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        TrophyTracker pct = (TrophyTracker)target;
        EditorGUILayout.BeginHorizontal();
        if (pct.currencyType == CurrencyType.XP)
        {
            if (GUILayout.Button("+5"))
            {
                Currency.Transaction(pct.currencyType, 5);
            }
            if (GUILayout.Button("-5"))
            {
                Currency.Transaction(pct.currencyType, -5);
            }
        }


        EditorGUILayout.EndHorizontal();
    }
}
#endif