using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformSetter : MonoBehaviour
{
    public RectTransform targetRect;

    private RectTransform selfRect;
    // Start is called before the first frame update
    private void OnEnable()
    {
        selfRect = GetComponent<RectTransform>();
    }

    private void Start()
    {
        selfRect.sizeDelta = new Vector2(targetRect.sizeDelta.x, selfRect.sizeDelta.y); 
    }
}
