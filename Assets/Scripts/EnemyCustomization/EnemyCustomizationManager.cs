using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy Customization Manager", menuName = "Enemy Customization Manager", order = 53)]

public class EnemyCustomizationManager : ScriptableObject
{
    public static EnemyCustomizationManager Instance { get; private set; }

    [SerializeField] private List<CustomEnemy> levelWiseCustomEnemy = new List<CustomEnemy>();

    public List<CustomEnemy> LevelWiseCustomEnemy
    {
        get => levelWiseCustomEnemy;
    }
}
