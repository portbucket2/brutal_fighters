using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomLimbRoot : MonoBehaviour
{
    public AnchoredJoint2D next_limb_joint;

    public List<CustomLimb> custom_limb;

    public void SetCustomLimb(int limb_skin_id)
    {
        foreach (CustomLimb item in custom_limb)
        {
            item.gameObject.SetActive(false);
        }

        custom_limb[limb_skin_id].gameObject.SetActive(true);

        if (next_limb_joint == null)
        {
            return;
        }
        Vector2 next_limb_anchor = next_limb_joint.connectedAnchor;

        if (custom_limb[limb_skin_id].next_is_horizontal)
        {
            next_limb_anchor.y = custom_limb[limb_skin_id].next_limb_anchor_x;
        }
        else
        {
            next_limb_anchor.x = custom_limb[limb_skin_id].next_limb_anchor_x;
        }

        next_limb_joint.connectedAnchor = next_limb_anchor;
    }

    public bool OutOfBounds(int id)
    {
        return (id >= custom_limb.Count || id < 0);
    }
}
