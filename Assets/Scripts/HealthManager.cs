using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthManager : MonoBehaviour
{
    public float max_health;
    public HealthBar health_bar;

    public bool is_dead = false;
    public bool can_execute_finisher = false;
    private bool finisher_invoked = false;

    private GameManager game_manager;

    private Tutorial_Fight tutorial;

    private void Start()
    {
        tutorial = FindObjectOfType<Tutorial_Fight>();

        game_manager = FindObjectOfType<GameManager>();
    }

    public void ReceiveDamage(float damage)
    {
        if (is_dead || can_execute_finisher || finisher_invoked)
        {
            return;
        }
        health_bar.UpdateHealth(-damage);

        if (tutorial != null)
        {
            tutorial.DragDone();
        }

        if (health_bar.GetHealthPercentage() <= 0f)
        {
            if (!finisher_invoked)
            {
                finisher_invoked = true;
                Enemy enemy = GetComponent<Enemy>();
                if (enemy != null)
                {
                    Invoke("AlmostDead", 1f);
                    enemy.FinisherInvoked();
                }
                else
                {
                    is_dead = true;
                    game_manager.Failed();
                }
            }
        }
    }

    private void AlmostDead()
    {
        can_execute_finisher = true;
        game_manager.FinishHim();

        Invoke("Finisher", 5f);
    }

    public void Finisher()
    {
        if (is_dead)
        {
            return;
        }

        is_dead = true;
        can_execute_finisher = false;

        if (Time.timeScale >= 0.9)
        {
            Time.timeScale = 0.3f;
            Invoke("DefaultSpeed", 0.8f);
        }
    }

    public float GetHealthPercentage()
    {
        return health_bar.GetHealthPercentage();
    }

    private void DefaultSpeed()
    {
        Time.timeScale = 1f;
        game_manager.LevelEnd();
    }
}
