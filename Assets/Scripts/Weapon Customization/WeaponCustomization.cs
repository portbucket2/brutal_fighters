using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WeaponCustomization : MonoBehaviour
{
    //[Header("Older")]

    public static Action WeaponPartDroppedAction;
    public static Action<WeaponPanel> WeaponPartPanelUpdated;

    public bool sendTrackingEvent = true;

    [Header("to keep")]
    public Gradient panel_color;
    //public SpriteRenderer panel_fill_L;
    //public SpriteRenderer panel_fill_R;

    public Vector3 spawn_offset;

    //public GameObject full_L;
    //public GameObject full_R;

    public ShopItemKeeper weapon_collection;

    public GameObject need_weapon;

    //internal List<WeaponPart> weapon_parts_L = new List<WeaponPart>();
    //internal List<WeaponPart> weapon_parts_R = new List<WeaponPart>();

    //public float max_weight_L;
    //private float cumulative_weight_L;
    //public GameObject panel_mask_L;
    //private Vector3 panel_L_target = new Vector3(0, 1, 1);

    //public float max_weight_R;
    //private float cumulative_weight_R;
    //public GameObject panel_mask_R;
    //private Vector3 panel_R_target = new Vector3(0, 1, 1);

    //public Transform Root_L_base;
    //public Transform Root_R_base;


    [Header("FRESH")]

    public GameObject lPanelCircleRoot;
    public GameObject rPanelCircleRoot;

    public WeaponCustomizationLoader leftLoader;
    public WeaponCustomizationLoader rightLoader;



    public RotationHandleManager rotation_handle_manager;


    public Action OnWeaponDroppedToPanel;

    void Start()
    {
        rotation_handle_manager = FindObjectOfType<RotationHandleManager>();
        WeaponData.Instance.LoadWeaponData();
        leftLoader.Init(WeaponData.Instance.customized_weapon_L, this);
        rightLoader.Init(WeaponData.Instance.customized_weapon_R, this);

        if (sendTrackingEvent)
        {
            CustomEventTracker.WeaponCustomizeOpened();
        }
    }


    public void SaveWeaponData()
    {
        WeaponData.Instance.SaveWeaponData();
    }

    public void RefreshCirclePositions()
    {
        if (leftLoader.GetLastPart())
        {
            Vector3 v = leftLoader.GetLastPart().top.position;
            v.z = lPanelCircleRoot.transform.position.z;
            lPanelCircleRoot.transform.position = v;
        }
        if (rightLoader.GetLastPart())
        {
            Vector3 v = rightLoader.GetLastPart().top.position;
            v.z = rPanelCircleRoot.transform.position.z;
            rPanelCircleRoot.transform.position = v;
        }
    }

    private void Update()
    {
        leftLoader.UpdateFillBar();
        rightLoader.UpdateFillBar();

        RefreshCirclePositions();
    }

    public void Play()
    {
        ApplyWeapon();
        WeaponData.Instance.SaveWeaponData();
        
        if (WeaponData.HasNoWeapon)
        {
            ShowNeedWeapon(true);
        }
        else
        {
            SceneManager.LoadScene(LevelDataController.CalculateNextLevel(0));
        }
    }

    public void ShowNeedWeapon(bool value)
    {
        need_weapon.SetActive(value);
    }

    //public void ResetWeapons()
    //{
    //    WeaponData.customized_weapon_L.Clear();
    //    WeaponData.customized_weapon_R.Clear();

    //    WeaponData.SaveWeaponData();
    //    SceneManager.LoadScene(1);
    //}

   

    private void OnApplicationQuit()
    {
        ApplyWeapon();
    }

    public void ApplyWeapon()
    {
        leftLoader.ApplyWeapon();
        rightLoader.ApplyWeapon();
    }
   
}