// Google Play Review Documentation: https://developer.android.com/guide/playcore/in-app-review/unity

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Google.Play.Review;

public class NativeInAppReview : MonoBehaviour
{
    // Create instance of ReviewManager
    private ReviewManager _reviewManager;
    private PlayReviewInfo _playReviewInfo;
    private void Start()
    {
        _reviewManager = new ReviewManager();
    }


    public void RequestNativeInAppReview()
    {

#if UNITY_ANDROID
        StartCoroutine(GooglePlayReview());
#elif UNITY_IOS
        UnityEngine.iOS.Device.RequestStoreReview();
#endif
    }


    IEnumerator GooglePlayReview()
    {
        var requestFlowOperation = _reviewManager.RequestReviewFlow();
        yield return requestFlowOperation;
        if (requestFlowOperation.Error != ReviewErrorCode.NoError)
        {
            // Log error. For example, using requestFlowOperation.Error.ToString().
            yield break;
        }
        _playReviewInfo = requestFlowOperation.GetResult();

        var launchFlowOperation = _reviewManager.LaunchReviewFlow(_playReviewInfo);
        yield return launchFlowOperation;
        _playReviewInfo = null; // Reset the object
        if (launchFlowOperation.Error != ReviewErrorCode.NoError)
        {
            // Log error. For example, using requestFlowOperation.Error.ToString().
            yield break;
        }
        // The flow has finished. The API does not indicate whether the user
        // reviewed or not, or even whether the review dialog was shown. Thus, no
        // matter the result, we continue our app flow.
    }
}
