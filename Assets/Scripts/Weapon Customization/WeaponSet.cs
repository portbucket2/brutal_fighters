using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WeaponSet
{
    public WingType side;
    public List<WeaponPart> defaultLevelWeapons;
    int sideID => (int)side;
    [NonSerialized] public List<SpawnWeaponData> spawn_data = new List<SpawnWeaponData>();


    public int Count => spawn_data.Count;
    public bool isEmpty => spawn_data.Count == 0;


    public void LoadInitialWeaponSet()
    {
        SpawnWeaponData weapon = new SpawnWeaponData();
        weapon.id = 3;
        weapon.position = Vector2.zero;
        weapon.rotation = 0;

        spawn_data.Add(weapon);

        SaveWeaponData();
    }

    public void LoadWeaponData()
    {
        spawn_data = GameData.LoadDataForJSON(sideID);
    }

    public void SaveWeaponData()
    {
        GameData.SaveDataForJSON(spawn_data, sideID);
    }
}
public enum WingType
{
    LEFT = 0,
    RIGHT = 1,
}
