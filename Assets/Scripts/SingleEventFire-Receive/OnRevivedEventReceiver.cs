using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnRevivedEventReceiver : MonoBehaviour
{
    public UnityEvent DoTheseOnRevived;

    private GameManager game_manager;
    void Start()
    {
        game_manager = FindObjectOfType<GameManager>();

        game_manager.OnRevived += OnRevivedEventReceived;
    }

    private void OnRevivedEventReceived()
    {
        DoTheseOnRevived?.Invoke();
    }
}
