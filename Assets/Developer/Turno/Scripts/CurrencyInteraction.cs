//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using TMPro;
//using System;
//using Text = TMPro.TextMeshProUGUI;
//#if UNITY_EDITOR
//using UnityEditor;
//#endif  

//public class CurrencyInteraction : MonoBehaviour
//{
//    #region Public Variables
//    //public Text currenyText;
//    public GameData gameData => GameData.Instance;
//    public ShopItemKeeper weaponCollection;
//    public static Action<int> onCurrentValueChange;
//    #endregion

//    #region Private Variables
//    #endregion
//    private void Start()
//    {
//        if (currenyText != null)
//        {
//            currenyText.text = gameData.Currency.ToString();
//            onCurrentValueChange?.Invoke(gameData.Currency);
//        }
//        else
//            Debug.Log("<color=red> Currency Text Reference Missing! </color>");
        

//    }
//    public void ChangeCurrencyValue(int value, InteractionType type)
//    {
//        gameData.ChangeValue( value, type);
//        currenyText.text = gameData.Currency.ToString();
//        onCurrentValueChange?.Invoke(gameData.Currency);
//    }


//    public bool ValidatePurchase(int value, InteractionType type)
//    {
//        return gameData.CurrenyValidation( value, type);
//    }
//}
//#if UNITY_EDITOR
//[CustomEditor(typeof(CurrencyInteraction))]
//public class CurrencyInteractionEditor : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        base.OnInspectorGUI();
//        CurrencyInteraction interaction = (CurrencyInteraction)target;
//        var upgradeStyle = new GUIStyle(GUI.skin.button);
//        upgradeStyle.normal.textColor = Color.green;

//        var downgradeStyle = new GUIStyle(GUI.skin.button);
//        downgradeStyle.normal.textColor = Color.white;
//        EditorGUILayout.BeginHorizontal();
//        if (GUILayout.Button("Coin Increase + " +1000, upgradeStyle) && Application.isPlaying)
//        {
//            interaction.gameData.ChangeValue( 1000, InteractionType.ADD);
//            interaction.currenyText.text = interaction.gameData.Currency.ToString();
//        }
//        if (GUILayout.Button("Health Upgrade + " + ConstantManager.healthUpgradeBasicValue, upgradeStyle) && Application.isPlaying)
//        {
//            interaction.gameData.UpgradeHealth();
//        }
//        if (GUILayout.Button("Health Downgrade - " + ConstantManager.healthUpgradeBasicValue, downgradeStyle) && Application.isPlaying)
//        {
//            interaction.gameData.UpgradeHealth();
//        }
//        EditorGUILayout.EndHorizontal();
//    }

//}

//#endif
