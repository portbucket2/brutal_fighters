using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PotatoSDK;

public class Settings : MonoBehaviour
{
    public GameObject settingsPanel;

    void Start()
    {
        if (!GDPRMan.ConsentApplies)
        {
            gameObject.SetActive(false);
        }
    }

    public void Privacy()
    {
        GDPRMan.RequestGDPRFlow();
    }

    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void Resume()
    {
        Time.timeScale = 1;
    }

}
