using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSpawner : MonoBehaviour
{
    public LightSaber customized_weapon;
    public List<GameObject> weapon_parts;
    public Transform spawn_root;
    public bool is_left;

    WeaponSet weaponSet;

    [SerializeField] float weapon_mass_min;
    [SerializeField] float weapon_mass_max;
    [SerializeField] float weapon_gravity_min;
    [SerializeField] float weapon_gravity_max;

    private void Awake()
    {
        customized_weapon.attack_damage = 8;
        weaponSet = is_left ? WeaponData.Instance.customized_weapon_L : WeaponData.Instance.customized_weapon_R;
        //List<WeaponPart>  releventDefaults = is_left ? default_weapon_L : default_weapon_R;

        float total_weight = 0;

        if (LevelDataController.currentFakeLevel == 1)
        {
            return;
        }

        if (GameData.Instance.player_knows_crafting.value == 0)
        {
            weaponSet.spawn_data.Clear();
            WeaponPart weapon_part = weaponSet.defaultLevelWeapons[LevelDataController.CalculateLevelRotation() - 1];
            SpawnWeaponData swd = new SpawnWeaponData(weapon_part);
            weaponSet.spawn_data.Add(swd);
        }

        foreach (SpawnWeaponData weapon_part in weaponSet.spawn_data)
        {
            //if (!weapon_part.connected)
            //{
            //    continue;
            //}
            GameObject spawned_weapon_part;
            spawned_weapon_part = Instantiate(weapon_parts[weapon_part.id]);
            spawned_weapon_part.transform.SetParent(spawn_root);

            spawned_weapon_part.transform.localEulerAngles = Vector3.forward * weapon_part.rotation;
            spawned_weapon_part.transform.localPosition = weapon_part.position;
            spawned_weapon_part.transform.localScale = Vector3.one;

            if(is_left) spawned_weapon_part.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 3;

            //customized_weapon.attack_damage += weapon_part.damage * Stats.player_weapon_part_damage_multiplier;

            total_weight += Global.Instance.weaponPartsUI[weapon_part.id].GetWeight();
        }

        Rigidbody2D rb = customized_weapon.GetComponent<Rigidbody2D>();

        rb.mass = Mathf.Lerp(weapon_mass_min, weapon_mass_max, total_weight / 6f);
        rb.gravityScale = Mathf.Lerp(weapon_gravity_min, weapon_gravity_max, total_weight / 6f);
    }
}
