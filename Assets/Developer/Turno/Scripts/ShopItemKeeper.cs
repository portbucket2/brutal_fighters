using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using FRIA;
[CreateAssetMenu(fileName = "New Weapon Collection", menuName = "Weapon Collection", order = 52)]
public class ShopItemKeeper : ScriptableObject
{
    public static ShopItemKeeper Instance { get; private set; }

    public List<ShopItemSO> smallWeaponCollection = new List<ShopItemSO>();
    public List<ShopItemSO> mediumWeaponCollection = new List<ShopItemSO>();
    public List<ShopItemSO> largeWeaponCollection = new List<ShopItemSO>();
    public List<ShopItemSO> armCollection = new List<ShopItemSO>();
    public List<ShopItemSO> legCollection = new List<ShopItemSO>();
    public List<ShopItemSO> headCollection = new List<ShopItemSO>();
    public List<ShopItemSO> torsoCollection = new List<ShopItemSO>();
    public List<ShopItemSO> unlockedByTrophyCollection = new List<ShopItemSO>();

    public HardData<int> itemIdForTrophy;

    [SerializeField] private List<ShopItemSO> weaponCollection = new List<ShopItemSO>();

    public static event Action<ShopItemType, int> onEquipedItemChanged;
    public List<ShopItemSO> GetWeaponCollection
    {
        get => weaponCollection;
    }




    public Dictionary<ShopItemType, List<ShopItemSO>> shopDictionary = new Dictionary<ShopItemType, List<ShopItemSO>>();
    public Dictionary<ShopItemType, HardData<int>> shopSelections = new Dictionary<ShopItemType, HardData<int>>();
    public static int GetEquipedID(ShopItemType type)
    {
        if (Instance.shopSelections.ContainsKey(type))
        {
            //$"Selection Id {Instance.shopSelections[type].value}".Debug("FF00FF"); 
            return Instance.shopSelections[type].value;
        }

        return -1;
    }
    public static  ShopItemSO GetSelectionItem(ShopItemType type)
    {
        int id = GetEquipedID(type);
        if (id >= 0)
        {
            foreach (var item in Instance.shopDictionary[type])
            {
                if (item.GetId == id)
                {
                    return item;
                }
            }
        }
        return null;
    }

    public static int GetLimbSelection(LimbType type)
    {
        
        switch (type)
        {
            case LimbType.LeftArm:
            case LimbType.LeftForeArm:
            case LimbType.RightArm:
            case LimbType.RightForeArm:
                return GetEquipedID(ShopItemType.ARM);
            case LimbType.LeftThigh:
            case LimbType.LeftShin:
            case LimbType.RightThigh:
            case LimbType.RightShin:
                return GetEquipedID(ShopItemType.LEG);
            case LimbType.Head:
                return GetEquipedID(ShopItemType.HEAD);
            case LimbType.Chest:
            case LimbType.Waist:
                return GetEquipedID(ShopItemType.TORSO);
            default:
                Debug.LogError("This is not a Limb!");
                break;
        }

        return -1;
    }

    [NonSerialized]
    bool initialized = false;
    public void EnsureInit()
    {
        if (initialized) return;
        Instance = this;

        weaponCollection.Clear();
        weaponCollection.AddRange(smallWeaponCollection);
        weaponCollection.AddRange(mediumWeaponCollection);
        weaponCollection.AddRange(largeWeaponCollection);

        itemIdForTrophy = new HardData<int>("TROPHY_UNLOCK", 0);

        foreach (ShopItemType siType in Enum.GetValues(typeof(ShopItemType)))
        {
            List<ShopItemSO> currentList=null;
            
            bool selectable = false;
            switch (siType)
            {
                case ShopItemType.SMALL:
                    currentList = smallWeaponCollection;
                    break;
                case ShopItemType.MEDIUM:
                    currentList = mediumWeaponCollection;
                    break;
                case ShopItemType.LARGE:
                    currentList = largeWeaponCollection;
                    break;             
                case ShopItemType.HEAD:
                    currentList = headCollection;
                    selectable = true;
                    break;
                case ShopItemType.TORSO:
                    currentList = torsoCollection;
                    selectable = true;
                    break;
                case ShopItemType.ARM:
                    currentList = armCollection;
                    selectable = true;
                    break;
                case ShopItemType.LEG:
                    currentList = legCollection;
                    selectable = true;
                    break;
                default:
                    break;
            }
            shopDictionary.Add(siType, currentList);
            foreach (var si in currentList)
            {
                if (si.IsDefault)
                {
                    HardData<int> hd =  new HardData<int>($"SELECTED_{siType}",si.GetId);
                    shopSelections.Add(siType,hd);
                    break;
                }
            }
            foreach (var si in currentList)
            {
                si.isSelectable = selectable;
            }
        }


        initialized = true;
    }

    public void OnEquip(ShopItemType type, int id)
    {
        shopSelections[type].value = id;
        onEquipedItemChanged?.Invoke(type, id);
    }
    public int GetAchievementTargetID
    {
        get => itemIdForTrophy.value;
    }
    public void IncrementTarget()
    { 
        if (GetAchievementTargetID < unlockedByTrophyCollection.Count - 1)
        {
            itemIdForTrophy.value += 1;
        }    
    }
}
public enum ShopItemType
{
    SMALL = 0,
    MEDIUM = 1,
    LARGE = 2,

    HEAD = 10,
    TORSO = 20,
    ARM = 30,
    LEG = 40,
    
}
public enum LimbType
{
    LeftArm = 0,
    LeftForeArm = 1,
    RightArm = 2,
    RightForeArm = 3,
    LeftThigh = 4,
    LeftShin = 5,
    RightThigh = 6,
    RightShin = 7,
    Head = 8,
    Chest = 9,
    Waist = 10,
}