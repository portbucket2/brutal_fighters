using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomWithSreenSize : MonoBehaviour
{
    public float zoom_multiplier;
    public float multiplier_factor;

    public float min_zoom;
    public float max_zoom;
    public bool cameraMovable = false;
    public float min_camX = 1.17f;
    public float max_camX = 4f;

    Camera cam;
    void Start()
    {
        cam = Camera.main;

        cam.orthographicSize = Mathf.Clamp(((float)Screen.height / Screen.width) * zoom_multiplier * multiplier_factor, min_zoom, max_zoom);

        //Debug.LogError(Screen.width + "X" + Screen.height);
        if (cameraMovable)
        {
            float ratio = (float)Screen.height / Screen.width;
            if (ratio < 0.56f)
                ratio = 0f;
            float xPos = Mathf.Lerp(min_camX, max_camX, ratio);
            Vector3 pos = cam.transform.localPosition;
            pos.x = xPos;
            cam.transform.localPosition = pos;
        }
        
    }

}
