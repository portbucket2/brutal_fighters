using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FRIA
{
    public static class GameObjectExtender
    {
        public static void ActivateAll(this List<GameObject> gameObjects)
        {
            foreach (var item in gameObjects)
            {
                item.SetActive(true);
            }
        }
        public static void DeactivateAll(this List<GameObject> gameObjects)
        {
            foreach (var item in gameObjects)
            {
                item.SetActive(false);
            }

        }
    }
}