using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
using UnityEngine.SceneManagement;

public class WeaponShopUI : ShopBehavior
{

    protected override void Awake()
    {
        base.Awake();

    }
    protected override void Start()
    {
        base.Start();
        //ShopItemKeeper.onEquipedItemChanged += OnEquipedItemChanged;

        TabContent[0].GetChild(0).GetComponent<Button>().onClick.Invoke();

        ContentPreview.weaponPurchasedAction += WeaponPurchased;

        CustomEventTracker.WeaponShopOpened();
    }

    void WeaponPurchased(ShopItemSO itemData)
    {
        OnItemPurchase(itemData, itemData.GetId);
    }


    public override void OnDestroy()
    {
        base.OnDestroy();

        ContentPreview.weaponPurchasedAction -= WeaponPurchased;
        //ContentPreview.itemPurchasedListener -= OnItemPurchase;
    }

    private void OnEquipedItemChanged(ShopItemType arg1, int arg2)
    {
        //throw new NotImplementedException();
    }

    protected override void OnTabChanged(int index)
    {
        base.OnTabChanged(index);
        TabChangeBehavior(index);

    }
    public override void OnItemPurchase(ShopItemSO itemData, int itemId)
    {
        GameData.Instance.player_knows_weapon_buying.value = 1;

        //itemCollection.OnEquip(itemData.GetShopItemType, itemId);
        CheckCostAvailability();

        ContentPreview.itemPurchased_Johnny_Potti?.Invoke();

        switch (itemData.GetUnlockMethod)
        {
            case ShopItemSO.ItemUnlockMethod.Ads:
                CustomEventTracker.WeaponPurchasedRV(itemData.GetWeaponName);
                break;

            case ShopItemSO.ItemUnlockMethod.Coins:
                CustomEventTracker.WeaponPurchasedCoin(itemData.GetWeaponName);
                break;

            default:
                break;
        }

    }

    public override void OnItemSelection(ShopItemSO item, ContentPreview contentPreview)
    {
        HideSelectedHighlightForAll();
        contentPreview.SetSelectionHighlightStatus(true);
        bool isPurchased = item.IsPurchased;
        if (isPurchased)
        {
            $"{item.GetWeaponName} is already purchased".Debug("FF00FF");
            //itemCollection.OnEquip(item.GetShopItemType, item.GetId);
        }
    }

    private void HideSelectedHighlightForAll()
    {
        for (int i = 0; i < TabContent[currentTabIndex].childCount; i++)
        {
            TabContent[currentTabIndex].GetChild(i).GetComponent<ContentPreview>().SetSelectionHighlightStatus(false);
        }
    }
    private void TabChangeBehavior(int index)
    {
        switch (index)
        {
            case 0:
            case 1:
            case 2:
                TabContent[index].GetChild(index).GetComponent<Button>().onClick.Invoke();
                break;
        }
    }
   

    public override void PopulateTabContent()
    {
        var shopDictenary = itemCollection.shopDictionary;
        if (shopDictenary.Count == 0) Debug.LogError("Shop Dictionary is Empty");
        int i = 0;
        foreach (var list in shopDictenary)
        {
            switch (list.Key)
            {
                case ShopItemType.SMALL:
                case ShopItemType.MEDIUM:
                case ShopItemType.LARGE:
                    foreach (ShopItemSO item in shopDictenary[list.Key])
                    {
                        ContentPreview content = Instantiate(itemPrefab, TabContent[i]).GetComponent<ContentPreview>();
                        content.LoadData(item: item);
                    }
                    i++;
                    break;
                default:
                    break;
            }

        }
    }
}
