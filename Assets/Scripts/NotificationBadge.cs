using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NotificationBadge : MonoBehaviour
{
    public List<ShopItemSO> itemsFilter;

    public GameObject badgeRoot;
    public TMP_Text badgeText;

    private void Awake()
    {
        Global.CoinChangedAction += CheckIfItemsPurchasable;

        CheckIfItemsPurchasable();
    }
    
    private void OnDestroy()
    {
        Global.CoinChangedAction -= CheckIfItemsPurchasable;
    }

    private void CheckIfItemsPurchasable()
    {
        int totalItemsPurchasable = 0;

        for (int i = 0; i < itemsFilter.Count; i++)
        {
            if (itemsFilter[i].IsDefault || itemsFilter[i].IsPurchased || itemsFilter[i].GetUnlockMethod != ShopItemSO.ItemUnlockMethod.Coins)
            {
                continue;
            }


            if (itemsFilter[i].GetCost <= Currency.Balance(CurrencyType.COIN))
            {
                totalItemsPurchasable++;
            }
        }

        if (totalItemsPurchasable == 0 || LevelDataController.currentFakeLevel < 4)
        {
            badgeRoot.SetActive(false);
        }
        else
        {
            badgeRoot.SetActive(true);
            badgeText.text = totalItemsPurchasable.ToString();
        }
    }
}
