using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
using UnityEngine.SceneManagement;

public class CharacterShop : ShopBehavior
{
    public Button crossButton;
    protected override void Awake()
    {
        base.Awake();

    }

    protected override void Start()
    {
        base.Start();
        ShopItemKeeper.onEquipedItemChanged += OnEquipedItemChanged;
        crossButton.onClick.AddListener(CloseShop);

        TabContent[0]
                    .GetChild(itemCollection.shopSelections[ShopItemType.HEAD]).GetComponent<Button>().onClick.Invoke();
        TabContent[0].GetChild(itemCollection.shopSelections[ShopItemType.HEAD])
            .GetComponent<ContentPreview>().EquippedTick(true);


        CustomEventTracker.BodyShopOpened();
    }

    

    public override void OnDestroy()
    {
        base.OnDestroy();
        ShopItemKeeper.onEquipedItemChanged -= OnEquipedItemChanged;
        crossButton.onClick.RemoveListener(CloseShop);
    }

    protected override void OnTabChanged(int index)
    {
        base.OnTabChanged(index);
        TabChangeBehavior(index);

    }
    public override void OnItemPurchase(ShopItemSO itemdata, int itemId)
    {
        itemCollection.OnEquip(itemdata.GetShopItemType, itemId);
        CheckCostAvailability();

        ContentPreview.itemPurchased_Johnny_Potti?.Invoke();

        switch (itemdata.GetUnlockMethod)
        {
            case ShopItemSO.ItemUnlockMethod.Ads:
                CustomEventTracker.LimbPurchasedRV(itemdata.GetWeaponName);
                break;

            case ShopItemSO.ItemUnlockMethod.Coins:
                CustomEventTracker.LimbPurchasedCoin(itemdata.GetWeaponName);
                break;

            case ShopItemSO.ItemUnlockMethod.Free:
                break;
            default:
                break;
        }
    }

    public override void OnItemSelection(ShopItemSO item, ContentPreview contentPreview)
    {
        HideSelectedHighlightForAll();
        contentPreview.SetSelectionHighlightStatus(true);
        bool isPurchased = item.IsPurchased;       
        if (isPurchased)
        {
            $"{item.GetWeaponName} is already purchased".Debug("FF00FF");
            itemCollection.OnEquip(item.GetShopItemType, item.GetId);
        }
    }

    private void HideSelectedHighlightForAll()
    {
        for (int i = 0; i < TabContent[currentTabIndex].childCount; i++)
        {
            TabContent[currentTabIndex].GetChild(i).GetComponent<ContentPreview>().SetSelectionHighlightStatus(false);
        }
    }
    private void TabChangeBehavior(int index)
    {
        switch (index)
        {
            case 0:
                TabContent[index]
                    .GetChild(itemCollection.shopSelections[ShopItemType.HEAD]).GetComponent<Button>().onClick.Invoke();
                TabContent[index].GetChild(itemCollection.shopSelections[ShopItemType.HEAD])
                    .GetComponent<ContentPreview>().EquippedTick(true);
                break;
            case 1:
                TabContent[index]
                    .GetChild(itemCollection.shopSelections[ShopItemType.TORSO]).GetComponent<Button>().onClick.Invoke();
                TabContent[index].GetChild(itemCollection.shopSelections[ShopItemType.TORSO])
                    .GetComponent<ContentPreview>().EquippedTick(true);
                break;
            case 2:
                TabContent[index]
                    .GetChild(itemCollection.shopSelections[ShopItemType.ARM]).GetComponent<Button>().onClick.Invoke();
                TabContent[index].GetChild(itemCollection.shopSelections[ShopItemType.ARM])
                    .GetComponent<ContentPreview>().EquippedTick(true);
                break;
            case 3:
                TabContent[index]
                    .GetChild(itemCollection.shopSelections[ShopItemType.LEG]).GetComponent<Button>().onClick.Invoke();
                TabContent[index].GetChild(itemCollection.shopSelections[ShopItemType.LEG])
                    .GetComponent<ContentPreview>().EquippedTick(true);
                break;
        }
    }
    public override void PopulateTabContent()
    {
        var selectableOnlyDictionary = itemCollection.shopDictionary;
        if (selectableOnlyDictionary.Count == 0) Debug.LogError("Shop Dictionary is Empty");
        int i = 0;
        foreach (var list in selectableOnlyDictionary)
        {
            switch (list.Key)
            {
                case ShopItemType.HEAD:
                case ShopItemType.TORSO:
                case ShopItemType.ARM:
                case ShopItemType.LEG:
                    foreach (ShopItemSO item in selectableOnlyDictionary[list.Key])
                    {
                        ContentPreview content = Instantiate(itemPrefab, TabContent[i]).GetComponent<ContentPreview>();
                        content.LoadData(item: item);
                    }
                    i++;
                    break;
                default:
                    break;
            }

        }
    }
    private void OnEquipedItemChanged(ShopItemType itemType, int id)
    {
        for (int i = 0; i < TabContent[currentTabIndex].childCount; i++)
        {
            TabContent[currentTabIndex].GetChild(i)
           .GetComponent<ContentPreview>().EquippedTick(false);
        }
        TabContent[currentTabIndex].GetChild(itemCollection.shopSelections[itemType])
           .GetComponent<ContentPreview>().EquippedTick(true);
    }
    private void CloseShop()
    {
        SceneManager.LoadScene(LevelDataController.CalculateNextLevel(0));
    }

}
