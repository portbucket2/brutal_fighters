using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSelector : MonoBehaviour
{
    public GameObject rotation_handle;
    public bool can_rotate;

    private RotationHandleManager rotation_handle_manager;

    private void Start()
    {
        rotation_handle_manager = FindObjectOfType<RotationHandleManager>();
    }

    private void OnMouseDown()
    {
        if (!can_rotate)
        {
            return;
        }
        rotation_handle_manager.SelectObject(rotation_handle);
    }
}
