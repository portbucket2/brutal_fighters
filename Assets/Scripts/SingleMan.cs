using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleMan : MonoBehaviour
{
    public static SingleMan Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        
    }
}
