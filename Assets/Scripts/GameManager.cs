using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using TMPro;
using GameAnalyticsSDK;
using LionStudios.Suite.Analytics;
using ByteBrewSDK;
using PotatoSDK;

public class GameManager : MonoBehaviour
{
    public GameObject rv_video_icon;
    public GameObject rv_free_icon;
    public GameObject nextButton;
    public GameObject currencyTracker;

    public GameObject leaderboardRoot;
    public LeaderboardManager leaderboardManager;
    
    public GameObject BattlepassRoot;

    public UnityEvent OnFinishHimStarted;

    public static int attemptNum = 1;
    public const float BASE_COIN_BONUS = 120;
    //public static GameManager Instance { get; private set; }

    public bool fight_started = false;

    public List<GameObject> show_on_fight_started_if_1;
    public List<GameObject> hide_on_fight_started;

    public TextMeshProUGUI coin_X;
    public TextMeshProUGUI coin_plus;
    public TextMeshProUGUI rv_coin;

    public TMP_Text level_num;
    public GameObject finish_him_text;
    public GameObject victory_text;
    public GameObject fail_ui;

    public float map_left_limit;
    public float map_right_limit;

    public int slow_down_counter;

    public bool level_completed = false;
    public bool level_failed = false;

    private Enemy enemy;
    public PlayerController player;
    private PlayerController player_backup;
    public HealthBar player_healthbar;

    public Tutorial_Fight tutorial;

    public Action OnStartFightEvent;
    public Action OnRevived;
    public Action<bool> OnReviveRVComplete;
    public Action<bool> OnCoinRVComplete;

    public Action OnLevelComplete;

    public void StartFight()
    {
        fight_started = true;
        
        OnStartFightEvent?.Invoke();

        foreach (GameObject item in hide_on_fight_started)
        {
            item.SetActive(false);
        }

        if (LevelDataController.currentFakeLevel == 1)
        {
            foreach (GameObject item in show_on_fight_started_if_1)
            {
                item.SetActive(true);
            }
        }
        else
        {
            enemy.RandomAction();
        }

    }

    private void OnApplicationQuit()
    {
        WeaponData.Instance.SaveWeaponData();
    }

    private void Awake()
    {
        OnReviveRVComplete += Revive;
        OnCoinRVComplete += CoinRV;

        //Instance = this;
        //DontDestroyOnLoad(gameObject);

        enemy = FindObjectOfType<Enemy>();


        if (enemy != null)
        {
            enemy.player_transform = player.transform;
        }



        //if (Stats.currentFakeLevel == 1)
        //{
        //    //WeaponData.SaveWeaponData(currency.persistantData);
        //}
        //else
        //{
        //    WeaponData.LoadWeaponData(currency.persistantData);
        //}
    }

    private void Start()
    {
        SetBackupForRevive();
        //LionAnalytics.GameStart();

        level_num.text = "Level " + (LevelDataController.currentFakeLevel).ToString();
        Application.targetFrameRate = 60;

        PlayerPrefs.SetInt("CurrentFakeLevel", LevelDataController.currentFakeLevel);
        LionAnalytics.LevelStart(LevelDataController.currentFakeLevel, attemptNum);
        ByteBrew.NewProgressionEvent(ByteBrewProgressionTypes.Started, "Level", LevelDataController.currentFakeLevel.ToString());

        CustomEventTracker.CheckCoinAtThisLevel();

        if (LevelDataController.currentFakeLevel == 4 && GameData.Instance.in_app_review_pressed_once == 0)
        {
            Global.InAppReviewAction?.Invoke();
        }
    }

    public void SetBackupForRevive()
    {
        player_backup = Instantiate(player);
        player_backup.gameObject.SetActive(false);
    }


    public void RevivePressed()
    {
        MAXMan.ShowRV(OnReviveRVComplete);
    }
    
    private void Revive(bool rv_complete)
    {
        if (!rv_complete)
        {
            //Debug.LogError("RV Cancelled");
            return;
        }
        //Debug.LogError("RV COMPLETE!");

        //player.gameObject.SetActive(false);
        player_backup.transform.position = player.transform.position;

        Destroy(player.gameObject);

        player = player_backup;

        player.gameObject.SetActive(true);

        enemy.player_transform = player.transform;

        level_failed = false;
        fail_ui.SetActive(false);
        player.ShowOnStartFight();

        OnRevived?.Invoke();

        player.transform.position += Vector3.left * 6f;
        player.intended_position = player.transform.position;

        player_healthbar.ResetHealthBar();

        CustomEventTracker.ReviveRVWatched();
    }

    public void CoinRVPressed()
    {
        if (GameData.Instance.got_free_rv_coin_once == 0)
        {
            GameData.Instance.got_free_rv_coin_once.value = 1;
            
            CoinRV(true);
        }
        else
        {
            MAXMan.ShowRV(OnCoinRVComplete);
        }
    }

    public void CoinRV(bool rv_complete)
    {
        if (!rv_complete)
        {
            return;
        }

        CustomEventTracker.CoinRVWatched();

        int totalCoin = (int)(BASE_COIN_BONUS * GameData.Instance.EarningMultiplier);

        Currency.Transaction(CurrencyType.COIN, totalCoin);

        //LeaderboardInit();
        BattlepassInit();
    }

    private void ShowNextButton()
    {
        nextButton.SetActive(true);
    }

    public void Restart()
    {
        Time.timeScale = 1f;

        LionAnalytics.LevelRestart(LevelDataController.currentFakeLevel, attemptNum);
        ByteBrew.NewCustomEvent("Restarted", LevelDataController.currentFakeLevel.ToString());

        attemptNum++;

        if (LevelDataController.currentFakeLevel >= 4)
        {
            MAXMan.ShowInterstitial();
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void FinishHim()
    {
        finish_him_text.SetActive(true);

        OnFinishHimStarted?.Invoke();
    }

    public void LevelEnd()
    {
        if (level_failed)
        {
            return;
        }

        OnLevelComplete?.Invoke();

        finish_him_text.SetActive(false);
        victory_text.SetActive(true);

        level_completed = true;

        int totalCoin = (int)(BASE_COIN_BONUS * GameData.Instance.EarningMultiplier);

        Currency.Transaction(CurrencyType.COIN, totalCoin);
        //currency.gameData.Currency += (int)BASE_COIN_BONUS * (int)currency.gameData.UpgradeMultiplier;

        coin_X.text = BASE_COIN_BONUS.ToString() + " x " + GameData.Instance.EarningMultiplier + " =";
        coin_plus.text = (totalCoin).ToString("F0");
        rv_coin.text = (totalCoin * 2).ToString("F0");


        if (LevelDataController.currentFakeLevel >= 3)
        {
            MAXMan.ShowInterstitial();
        }

        rv_free_icon.SetActive(GameData.Instance.got_free_rv_coin_once.value == 0);
        rv_video_icon.SetActive(GameData.Instance.got_free_rv_coin_once.value == 1);

        if (LevelDataController.currentFakeLevel != 1)
        {
            Invoke("ShowNextButton", 3f);
        }

        Global.CoinPlusAction?.Invoke(currencyTracker.transform.position);

        LionAnalytics.LevelComplete(LevelDataController.currentFakeLevel, attemptNum);
        ByteBrew.NewProgressionEvent(ByteBrewProgressionTypes.Completed, "Level", LevelDataController.currentFakeLevel.ToString());
    }

    public void Next()
    {
        attemptNum = 1;

        SceneManager.LoadScene(LevelDataController.CalculateNextLevel(1));
        //Stats.currentFakeLevel += 1;
        //SceneManager.LoadScene(1);
    }

    public void BattlepassInit()
    {
        BattlepassRoot.SetActive(true);

        Currency.Transaction(CurrencyType.XP, 5);
    }

    public void LeaderboardInit()
    {
        leaderboardRoot.SetActive(true);

        int rankIncrease = 0;

        int playerRank = GameData.Instance.player_leaderboard_rank.value;

        leaderboardManager.Initiate();

        if (level_failed)
        {
            rankIncrease -= UnityEngine.Random.Range(-2, -6);
            
            leaderboardManager.RankDown(rankIncrease);
        }
        else
        {
            rankIncrease -= UnityEngine.Random.Range(2, 6);

            leaderboardManager.RankUp(rankIncrease);
        }
        
        playerRank += rankIncrease;

        if (playerRank >= 1)
        {
            GameData.Instance.player_leaderboard_rank.value = playerRank;
        }

    }

    public void Failed()
    {
        if (level_completed)
        {
            return;
        }

        level_failed = true;
        fail_ui.SetActive(true);

        LionAnalytics.LevelFail(LevelDataController.currentFakeLevel, attemptNum);
        ByteBrew.NewProgressionEvent(ByteBrewProgressionTypes.Failed, "Level", LevelDataController.currentFakeLevel.ToString());
    }

}
