using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject Enemy;

    public float dist_multiplier;

    public float lower_limit;
    public float upper_limit;

    public float speed;

    private Camera cam;

    private Vector3 offset;

    private GameManager game_manager;

    private void Start()
    {
        game_manager = FindObjectOfType<GameManager>();

        game_manager.OnRevived += OnRevivedEventReceived;

        cam = Camera.main;

        SetOffset();
    }

    void SetOffset()
    {
        offset = (Vector2)transform.position - (Vector2)game_manager.player.transform.position;

        offset.z = -10;
    }

    void OnRevivedEventReceived()
    {
        SetOffset();
    }

    void Update()
    {
        transform.position = game_manager.player.transform.position + offset;

        float dist = (game_manager.player.transform.position - Enemy.transform.position).magnitude;

        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, Mathf.Clamp(dist * dist_multiplier, lower_limit, upper_limit), Time.deltaTime * speed);
    }
}
