using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationHandleManager : MonoBehaviour
{
    private GameObject current_rotation_handle;

    public void SelectObject(GameObject rotation_handle)
    {
        if (current_rotation_handle != null)
        {
            current_rotation_handle.SetActive(false);
        }

        current_rotation_handle = rotation_handle;

        current_rotation_handle.SetActive(true);
    }
}
