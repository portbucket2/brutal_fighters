using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReceiver : MonoBehaviour
{
    public HealthManager parent_avatar;
    public bool receive_damage = true;

    GameManager game_manager;

    private void Awake()
    {
        game_manager = FindObjectOfType<GameManager>();
    }

    public void ReceiveDamage(float damage)
    {
        if (!receive_damage)
        {
            return;
        }

        if (!game_manager.fight_started)
        {
            return;
        }

        if (parent_avatar.can_execute_finisher)
        {
            parent_avatar.Finisher();
        }
        else
        {
            parent_avatar.ReceiveDamage(damage);
        }

    }
}
