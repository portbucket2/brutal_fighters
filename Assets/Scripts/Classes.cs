using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class RotationFollower
{
    public Rigidbody2D rb;
    public Rigidbody2D targetBody;
    public float force;

    public void FollowRotation()
    {
        if (rb.sharedMaterial == null)
        {
            rb.MoveRotation(Mathf.LerpAngle(rb.rotation, targetBody.rotation, force * Time.deltaTime));
        }
    }
}

[System.Serializable]
public class PositionFollower
{
    public Rigidbody2D rb;
    public Rigidbody2D targetBody;
    public float force;

    public void FollowPosition()
    {
        if (rb.sharedMaterial == null)
        {
            rb.MovePosition(Vector2.Lerp(rb.position, targetBody.position, force * Time.deltaTime));
        }
    }
}