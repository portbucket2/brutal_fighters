using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BloodToggleButton : MonoBehaviour
{
    public Color onColor;
    public Color offColor;

    Button btn;
    public TMP_Text text;

    private void Awake()
    {
        btn = GetComponent<Button>();
    }

    public void BloodTogglePressed()
    {
        Tweaker.show_blood = !Tweaker.show_blood;

        Global.ShowBloodAction?.Invoke(Tweaker.show_blood);

        if (Tweaker.show_blood)
        {
            btn.image.color = onColor;
            text.text = "Blood On";
        }
        else
        {
            btn.image.color = offColor;
            text.text = "Blood Off";
        }
    }
}
