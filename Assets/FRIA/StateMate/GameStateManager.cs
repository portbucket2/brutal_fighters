﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class GameStateManager : MonoBehaviour
{
    public static GameStateManager Instance { get; private set; }
    public static IStateMateAccess<GameState> stateAccess { get { return Instance.stateMate; } }
    public static GameState State { get { return stateAccess.CurrentState; } }
    public static bool inGame { get { return State == GameState.Game; } }
    
    private StateMate<GameState> stateMate;

    public List<GameObject> startObjs;
    public List<GameObject> purchaseObjs;
    public List<GameObject> inGameObjs;
    private void Awake()
    {
        Instance = this;
        stateMate = new StateMate<GameState>(initial: GameState.ReadyUp, enableDefinedTransitionOnly: true, loadEmpties: true);


        GameState[] mainLoop = new GameState[] 
        {
            GameState.ReadyUp,
            GameState.Game,
            GameState.Success,
        };
        stateMate.AddTransitionChain(mainLoop);
        stateMate.AddTransition(GameState.Game, GameState.Fail);
        stateMate.logStateSwitches = true;

    }
    private void Start()
    {
        stateMate.SetInitialSwitches();
    }
    public static void RequestSwitch(GameState gs)
    {
        Instance.stateMate.SwitchState(gs);
    }
}

public enum GameState
{
    
    ReadyUp = -1,
    Game = 0,
    Success = 1,
    Fail = 2,
}