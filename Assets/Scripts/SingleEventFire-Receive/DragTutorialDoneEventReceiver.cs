using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DragTutorialDoneEventReceiver : MonoBehaviour
{
    Tutorial_Fight tutorial;

    public UnityEvent DoTheseOnDragTutorialDone;

    private void Start()
    {
        tutorial = FindObjectOfType<Tutorial_Fight>();

        tutorial.OnDragTutDoneEvent += OnDragTutDoneEventReceived;
    }

    public void OnDragTutDoneEventReceived()
    {
        DoTheseOnDragTutorialDone?.Invoke();
    }

}
