using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial_Upgrade_Button : MonoBehaviour
{
    public UnityEvent events;

    private void Start()
    {
        if (LevelDataController.currentFakeLevel == 3 && GameData.Instance.player_knows_limb_upgrade == 0)
        {
            events?.Invoke();
        }
    }
}
