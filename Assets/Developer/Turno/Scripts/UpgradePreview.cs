using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public enum UpgradeType
{
    HEALTH,
    DAMAGE,
    COIN
}
public class UpgradePreview : MonoBehaviour
{
    public UpgradeType upgradeType;
    public Image preview;
    public static UnityAction<Image, UpgradeType> upgradeSelectedListener;

    public void OnSelection()
    {
        upgradeSelectedListener?.Invoke(preview, upgradeType);
    }
}
