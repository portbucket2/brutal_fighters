using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LeaderboardManager : MonoBehaviour
{
    [SerializeField] TMP_Text debugText;
    [SerializeField] bool testing = false;
    public bool contentShowPlayer;
    private bool unInitiated = false;

    public int playerRank;

    [SerializeField] ScrollRect scrollRect;

    [SerializeField] RandomNameGenerator randomNameGenerator;
    [SerializeField] List<LeaderboardPanel> leaderboardPanels;
    
    [Space]

    [SerializeField] RectTransform content;
    [SerializeField] LeaderboardPanel playerPanel;
    [SerializeField] LeaderboardPanel playerPanelAnimated;
    [SerializeField] Animator playerPanelAnimator;
    [SerializeField] float playerPanelSpeed;

    [Space]

    [SerializeField] Vector3 contentRootPosUpperLimit;
    [SerializeField] Vector3 contentRootPosLowerLimit;
    [SerializeField] float contentAnimSpeed;

    private Vector3 contentRootTargetPosition = Vector3.zero;

    [Space(15)]

    [Button("GoUpMore")]
    public bool goUpMore;
    public void GoUpMore()
    {
        playerPanelAnimator.Play("ZUp", -1, 0);

        GoUp();
        Invoke("GoUp", 0.1f);
        Invoke("GoUp", 0.2f);
        Invoke("GoUp", 0.3f);
    }


    [Space(5)]

    [Button("GoUp")]
    public bool goUp;
    public void GoUp()
    {
        if (playerPanel.transform.GetSiblingIndex() == 0)
        {
            return;
        }

        playerRank--;

        int newIndex = playerPanel.transform.GetSiblingIndex() - 1;
        
        //playerPanelAnimated.transform.parent.position = content.GetChild(playerPanel.transform.GetSiblingIndex() - 1).transform.position;

        newIndex = Mathf.Clamp(newIndex, 0, content.childCount);

        playerPanel.transform.SetSiblingIndex(newIndex);
        
        Refresh();
    }

    [Space(5)]

    [Button("GoDown")]
    public bool goDown;
    public void GoDown()
    {
        if (playerPanel.transform.GetSiblingIndex() == content.childCount - 1)
        {
            return;
        }

        playerRank++;

        int newIndex = playerPanel.transform.GetSiblingIndex() + 1;

        //playerPanelAnimated.transform.parent.position = content.GetChild(playerPanel.transform.GetSiblingIndex() + 1).transform.position;

        newIndex = Mathf.Clamp(newIndex, 0, content.childCount);

        playerPanel.transform.SetSiblingIndex(newIndex);

        Refresh();
    }

    [Space(5)]
    [Button("GoDownMore")]
    public bool goDownMore;
    public void GoDownMore()
    {
        playerPanelAnimator.Play("ZUp", -1, 0);

        GoDown();
        Invoke("GoDown", 0.1f);
        Invoke("GoDown", 0.2f);
        Invoke("GoDown", 0.3f);
    }

    int rankStep;

    public void RankUp(int value)
    {
        rankStep = Mathf.Abs(value);

        Invoke("RankUpDelayed", 0.85f);
    }

    private void RankUpDelayed()
    {
        if (!testing) unInitiated = false;
        playerPanelAnimator.Play("ZUp", -1, 0);

        float delay = 0.15f;
        float cumulativeDelay = 0f;

        for (int i = 0; i < rankStep; i++)
        {
            Invoke("GoUp", cumulativeDelay);

            cumulativeDelay += delay;
        }
    }

    public void RankDown(int value)
    {
        rankStep = Mathf.Abs(value);

        Invoke("RankDownDelayed", 0.85f);
    }

    public void RankDownDelayed(int value)
    {
        if (!testing) unInitiated = false;
        playerPanelAnimator.Play("ZUp", -1, 0);

        float delay = 0.15f;
        float cumulativeDelay = 0f;

        for (int i = 0; i < value; i++)
        {
            Invoke("GoDown", cumulativeDelay);

            cumulativeDelay += delay;
        }
    }

    private void Start()
    {
        

        Initiate();

    }

    private void RefreshContentRootPosition()
    {
        float interpolation = (float)playerPanel.transform.GetSiblingIndex() / content.transform.childCount;

        contentRootTargetPosition = Vector3.Lerp(contentRootPosUpperLimit, contentRootPosLowerLimit, interpolation);

        
    }

    private void Update()
    {
        //contentRootPosUpperLimit.y = Mathf.Abs(leaderboardPanels[0].transform.localPosition.y);
        //contentRootPosLowerLimit.y = Mathf.Abs(leaderboardPanels[leaderboardPanels.Count - 1].transform.localPosition.y);

        if (debugText) debugText.text = content.localPosition.ToString();

        if (contentShowPlayer)
        {
            if (unInitiated)
            {
                playerPanelAnimated.transform.parent.position = playerPanel.transform.position;

                content.localPosition = contentRootTargetPosition;
            }
            else
            {
                playerPanelAnimated.transform.parent.position = Vector3.Lerp(playerPanelAnimated.transform.parent.position, playerPanel.transform.position, Time.deltaTime * playerPanelSpeed);

                content.localPosition = Vector3.Lerp(content.localPosition, contentRootTargetPosition, Time.deltaTime * contentAnimSpeed);
            }
        }
        //content.position = playerPanel.transform.position;
    }

    public void AnimationFinished()
    {
        if (!testing) contentShowPlayer = false;
        scrollRect.enabled = true;
        //scrollRect.scrollSensitivity = 1;
    }

    public void Refresh()
    {
        int playerSiblingIdx = playerPanel.transform.GetSiblingIndex();
        int initRank = playerRank - playerSiblingIdx;
        int score = 10250 - initRank;
        int playerScore = score;

        int[] scores = new int[leaderboardPanels.Count];

        for (int i = 0; i < scores.Length; i++)
        {
            scores[i] = score;

            if (i == playerSiblingIdx)
            {
                playerScore = score;
            }

            score -= Random.Range(1, 10);
        }

        foreach (LeaderboardPanel item in leaderboardPanels)
        {
            int siblingIdx = item.transform.GetSiblingIndex();

            int rank = initRank + siblingIdx;
            
            item.Init(rank, scores[siblingIdx]);
        }


        playerPanelAnimated.Init(playerRank, playerScore);

        RefreshContentRootPosition();
    }

    public void Initiate()
    {
        if (!testing) unInitiated = true;
        //scrollRect.scrollSensitivity = 0;

        playerRank = GameData.Instance.player_leaderboard_rank.value;


        contentRootPosUpperLimit.x = content.localPosition.x;
        contentRootPosLowerLimit.x = content.localPosition.x;


        if (playerRank <= content.childCount)
        {
            playerPanel.transform.SetSiblingIndex(playerRank - 1);
        }
        
        RefreshContentRootPosition();
        

        foreach (LeaderboardPanel item in leaderboardPanels)
        {
            string personName = randomNameGenerator.GetRandomNameOnce();

            item.SetName(personName);
        }

        //contentRootPosLowerLimit.y = content.childCount * 146;

        Refresh();
    }

}
