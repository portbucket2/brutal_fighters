using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableWeaponPartPool : MonoBehaviour
{
    [SerializeField] List<WeaponPart> weapon_parts_to_spawn;
    public Transform spawn_root;

    public List<WeaponPool> weapon_pool;

    Dictionary<int,WeaponPart> weapon_spawn_dictionary;
    private void EnsureInit()
    {
        if (weapon_spawn_dictionary != null) return;

        weapon_spawn_dictionary = new Dictionary<int, WeaponPart>();
        foreach (WeaponPart item in weapon_parts_to_spawn)
        {
            weapon_spawn_dictionary.Add(item.id,item);
        }
    }

    public WeaponPart GetWeaponPartFromID(int id)
    {
        EnsureInit();
        WeaponPart weapon_part;

        //if (weapon_pool[id].pool.Count > 0)
        //{
        //    weapon_part = weapon_pool[id].GetWeaponPart();
        //}
        //else
        //{
        //    weapon_part = Instantiate(weapon_parts_to_spawn[id]);
        //}

        //weapon_part.transform.SetParent(spawn_root);
        //weapon_part.transform.localPosition = Vector3.zero;
        
        weapon_part = Instantiate(weapon_spawn_dictionary[id]);
        weapon_part.id = id;

        return weapon_part;

    }

    public void AddToPool(int id, WeaponPart weapon_part)
    {
        //weapon_pool[id].AddToPool(weapon_part);
    }
}

[System.Serializable]
public class WeaponPool
{
    public int current = 0;
    public List<WeaponPart> pool;

    public WeaponPart GetWeaponPart()
    {
        WeaponPart weapon_part = pool[current];

        current++;

        if (current >= pool.Count)
        {
            current = 0;
        }

        return weapon_part;
    }

    public void AddToPool(WeaponPart weapon_part)
    {
        pool.Add(weapon_part);
    }
}