using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial_CraftLearn : Tutorial
{
    WeaponCustomization weapon_customization;

    private bool weapon_dropped_once = false;

    private int tut_step = 1;

    public UnityEvent teach_drag_n_drop_step1;
    public UnityEvent teach_drag_n_drop_step2;
    public UnityEvent teach_drag_n_drop_step3;
    public UnityEvent after_weapon_drop_to_panel;

    private void Awake()
    {
        weapon_customization = FindObjectOfType<WeaponCustomization>();
    }

    public void Start()
    {
        if (GameData.Instance.player_knows_crafting.value == 0 && GameData.Instance.player_knows_weapon_buying.value == 1)
        {
            teach_drag_n_drop_step1?.Invoke();

            weapon_customization.OnWeaponDroppedToPanel += WeaponDroppedToPanel;
        }
    }


    private void WeaponDroppedToPanel()
    {
        if (tut_step > 3)
        {
            return;
        }

        tut_step++;

        if (tut_step == 2)
        {
            teach_drag_n_drop_step2?.Invoke();
        }

        if (tut_step == 3)
        {
            teach_drag_n_drop_step3?.Invoke();
        }
    }
}
