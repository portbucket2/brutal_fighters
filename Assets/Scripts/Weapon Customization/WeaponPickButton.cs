using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WeaponPickButton : MonoBehaviour
{
    [Header("VisualLoading")]
    public Image proPic;
    private bool drag_enabled = false;

    [Header("Management")]
    public bool locked = true;
    public GameObject lock_icon;

    public WeaponPanel defaultPanel = WeaponPanel.right;
    public int id;
    [HideInInspector][SerializeField]public float weight;

    //public ScrollRect scroll_rect;
    public float min_drag_to_spawn;
    public WeaponPart spawn_object;

    private BoxCollider2D col;

    private Vector2 drag_start_pos = Vector2.zero;
    private Vector2 last_mouse_pos = Vector2.zero;
    Vector2 drag_relative = Vector2.zero;

    private Vector3 touch_offset = Vector3.zero;
    private float touch_z_coord;
    Camera cam;
    EventSystem event_system;
    WeaponCustomization weapon_customization;
    DraggableWeaponPartPool weapon_pool;

    private bool dragged_out_once = false;

    ShopItemKeeper weapon_collection;

    private Vector2 drag_spawn_offset = Vector2.up * 10;

    WeaponPickButtonManager weapon_pick_button_manager;

    ScrollRect scrollRect;
    
    public void Load(ShopItemSO sitem, ScrollRect sr)
    {
        //scroll_rect = sr;
        gameObject.name = $"Weapon {sitem.GetId}";
        id = sitem.GetId;
        proPic.sprite = sitem.GetIcon;
        locked = !sitem.IsPurchased;

        lock_icon.SetActive(locked);
    }

    void Start()
    {
        cam = Camera.main;
        col = GetComponent<BoxCollider2D>();
        event_system = FindObjectOfType<EventSystem>();
        weapon_pool = FindObjectOfType<DraggableWeaponPartPool>();
        weapon_customization = FindObjectOfType<WeaponCustomization>();
        weapon_pick_button_manager = FindObjectOfType<WeaponPickButtonManager>();

        weapon_collection = weapon_customization.weapon_collection;

        //locked = !weapon_collection.GetWeaponCollection[id].PurchaseStatusProperty;

        //locked = !weapon_customization.currency.weaponCollection.GetWeaponCollection[id].IsPurchased;

        //lock_icon.SetActive(locked);

        if (locked)
        {
            //Vector2 pos = transform.localPosition;
            //pos.x = 0;
            //transform.localPosition = pos;
            //gameObject.SetActive(false);
        }
        else
        {
            //Vector2 pos = transform.localPosition;
            //pos.x = weapon_pick_button_manager.pos_x;

            //transform.localPosition = pos;

            //weapon_pick_button_manager.pos_x += 100;

            //weapon_pick_button_manager.IncreaseWidth();

            //Debug.LogError(name + " === " + transform.localPosition.x);
        }

        scrollRect = transform.parent.parent.parent.GetComponent<ScrollRect>();
    }
    public void Init()
    {
        spawn_object = weapon_pool.GetWeaponPartFromID(id);
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 touchpos = Input.mousePosition;

        touchpos.z = touch_z_coord;

        return cam.ScreenToWorldPoint(touchpos);
    }


    protected void OnMouseDown()
    {
        if (locked)
        {
            return;
        }

        last_mouse_pos = Input.mousePosition;
        drag_start_pos = Input.mousePosition;

        touch_z_coord = cam.WorldToScreenPoint(transform.position).z;

        touch_offset = transform.position - GetMouseWorldPos();
    }

    private void PlayerKnowsCrafting()
    {
        if (GameData.Instance.player_knows_crafting.value == 0)
        {
            GameData.Instance.player_knows_crafting.value = 1;

            CustomEventTracker.FirstTimeCrafting();
        }

    }
    public WeaponCustomizationLoader wcl_left => weapon_customization.leftLoader;
    public WeaponCustomizationLoader wcl_right => weapon_customization.rightLoader;




    protected void OnMouseUp()
    {
        if (locked)
        {
            return;
        }

        //Debug.LogError("DOWN");
        //col.enabled = true;
        //scroll_rect.horizontal = true;
        if (!dragged_out_once)
        {
            return;
        }


        WeaponAttachCheck attachCheck = spawn_object.weapon_attach_check;
        WeaponCustomizationLoader wcl = null;

        switch (attachCheck.weapon_panel)
        { 
            default:
            case WeaponPanel.left:
                wcl = wcl_left;
                break;
            case WeaponPanel.right:
                wcl = wcl_right;
                break;
        }
        System.Action<WeaponCustomizationLoader> act=null;

        if (wcl.PartCount == 0)
        {
            act = SnapToRoot;
        }
        else if (!spawn_object.weapon_attach_check.is_connected)
        {

            act = SnapToLast;
        }
        else
        {
            //act = SnapToLast;
            act = AttachOnPlace;

            spawn_object.GetComponent<DragToMove>().drag_enabled = drag_enabled;
            spawn_object.weapon_attach_check.is_first_part = false;
        }
        TryAddMore(wcl, act);

        dragged_out_once = false;

        spawn_object.weapon_attach_check.is_being_dragged = false;

        WeaponCustomization.WeaponPartDroppedAction?.Invoke();

        //weapon_customization.RefreshCirclePositions();

        drag_relative = Vector2.zero;

    }

    void TryAddMore(WeaponCustomizationLoader wcl, System.Action<WeaponCustomizationLoader> onSuccess, System.Action<WeaponCustomizationLoader> onFail = null)
    {
        if (!wcl.AddToWeight(spawn_object.weight))
        {
            spawn_object.gameObject.SetActive(false);
            weapon_pool.AddToPool(id, spawn_object);
            onFail?.Invoke(wcl);
        }
        else
        {
            onSuccess?.Invoke(wcl);
            weapon_customization.OnWeaponDroppedToPanel?.Invoke();
        }
    }
    void SnapToRoot(WeaponCustomizationLoader wcl)
    {
        spawn_object.transform.SetParent(wcl.root_base);
        spawn_object.transform.localScale = Vector3.one;
        spawn_object.transform.localPosition = Vector3.zero;
        spawn_object.transform.localEulerAngles = Vector3.zero;

        spawn_object.weapon_attach_check.is_first_part = true;
        spawn_object.GetComponent<DragToMove>().drag_enabled = false;
        spawn_object.GetComponent<ObjectSelector>().can_rotate = false;
        spawn_object.weapon_attach_check.SetConnected(true);

        wcl.AddPart(spawn_object);
        PlayerKnowsCrafting();
    }

    void SnapToLast(WeaponCustomizationLoader wcl)
    {
        spawn_object.transform.SetParent(wcl.root_base);
        WeaponPart lastPart = wcl.GetLastPart();
        lastPart.GetComponent<ObjectSelector>().can_rotate = false;
        spawn_object.transform.localScale = Vector3.one;
        spawn_object.transform.position = lastPart.top.position;
        spawn_object.transform.localEulerAngles = Vector3.zero;
        wcl.AddPart(spawn_object);
        PlayerKnowsCrafting();
        weapon_customization.rotation_handle_manager.SelectObject(spawn_object.GetComponent<ObjectSelector>().rotation_handle);
    }

    void AttachOnPlace(WeaponCustomizationLoader wcl)
    {
        spawn_object.transform.SetParent(wcl.root_base);
        spawn_object.transform.localScale = Vector3.one;
        spawn_object.transform.localEulerAngles = Vector3.forward * 20f;
        weapon_customization.rotation_handle_manager.SelectObject(spawn_object.GetComponent<ObjectSelector>().rotation_handle);
        wcl.AddPart(spawn_object);
        PlayerKnowsCrafting();
    }

    protected void DragStarted()
    {
        drag_start_pos = Input.mousePosition;
        last_mouse_pos = Input.mousePosition;
    }

    protected void DragEnded()
    {

    }

    protected void OnMouseDrag()
    {

        Vector2 drag_dist = (Vector2)Input.mousePosition - drag_start_pos;

        drag_relative = (Vector2)Input.mousePosition - last_mouse_pos;

        if (!dragged_out_once)
        {
            transform.parent.position += Vector3.up * drag_relative.y;
            if (scrollRect)
            {
                scrollRect.velocity = drag_relative * 50;
            }
        }
        //Debug.LogError(drag_dist.x);
        if (drag_dist.x < -70 && !locked)
        {
        
            if (!dragged_out_once)
            {
                dragged_out_once = true;
                spawn_object = weapon_pool.GetWeaponPartFromID(id);
                spawn_object.id = id;
                spawn_object.weapon_attach_check.weapon_panel = defaultPanel;

                spawn_object.weapon_attach_check.is_being_dragged = true;

                if (scrollRect)
                {
                    scrollRect.velocity = Vector2.zero;
                }
            }


            spawn_object.gameObject.SetActive(true);
            Vector3 pos = GetMouseWorldPos() + touch_offset + weapon_customization.spawn_offset;
            pos.z = 10;
            spawn_object.transform.position = pos;

            //col.enabled = false;
            //GUI.FocusControl(null);
            //scroll_rect.horizontal = false;
            PointerEventData data = new PointerEventData(event_system);
            //scroll_rect.OnEndDrag(data);
        }


        last_mouse_pos = Input.mousePosition;
    }
}
