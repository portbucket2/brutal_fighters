using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponPanel
{
    left,
    right
}

public class WeaponAttachCheck : MonoBehaviour
{
    public float alpha_when_disconnected = 0.7f;
    public PhysicsMaterial2D default_friction;
    public GameObject green_outline;
    public GameObject red_outline;

    private SpriteRenderer sprite;

    private int connected_body_count = 0;
    //private PolygonCollider2D col;
    private Rigidbody2D col;
    public bool is_connected = false;
    public bool is_first_part = false;

    public WeaponPanel weapon_panel = WeaponPanel.right;

    public bool is_being_dragged = false;

    private List<WeaponAttachCheck> collidedBodies = new List<WeaponAttachCheck>();

    private void Awake()
    {
        //col = GetComponent<PolygonCollider2D>();
        col = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();

        sprite.color = new Color(1, 1, 1, alpha_when_disconnected);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("L Panel"))
        {
            weapon_panel = WeaponPanel.left;
            if (is_being_dragged)
            {
                WeaponCustomization.WeaponPartPanelUpdated?.Invoke(weapon_panel);
            }
        }


        if (collision.CompareTag("R Panel"))
        {
            weapon_panel = WeaponPanel.right;
            if (is_being_dragged)
            {
                WeaponCustomization.WeaponPartPanelUpdated?.Invoke(weapon_panel);
            }
        }


        if (collision.CompareTag("Weapon"))
        {

            if (collision.attachedRigidbody.sharedMaterial == null && col.sharedMaterial == null)
            {
                return;
            }

            WeaponAttachCheck otherAtachCheck = collision.GetComponent<WeaponAttachCheck>();

            if (!collidedBodies.Contains(otherAtachCheck))
            {
                collidedBodies.Add(otherAtachCheck);
            }

            CheckIfConnected();

            //if (otherAtachCheck.weapon_panel == weapon_panel && !collidedBodies.Contains(otherAtachCheck))
            //{
            //    connected_body_count++;
            //    SetConnected(true);
            //}
        }
    }

    void CheckIfConnected()
    {
        bool check_result = false;

        foreach (WeaponAttachCheck item in collidedBodies)
        {
            if(item.weapon_panel == weapon_panel)
            {
                check_result = true;
            }
        }

        SetConnected(check_result);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("L Panel"))
        {
            weapon_panel = WeaponPanel.right;
            if (is_being_dragged)
            {
                WeaponCustomization.WeaponPartPanelUpdated?.Invoke(weapon_panel);
            }
        }


        if (collision.CompareTag("R Panel"))
        {
            weapon_panel = WeaponPanel.left;
            if (is_being_dragged)
            {
                WeaponCustomization.WeaponPartPanelUpdated?.Invoke(weapon_panel);
            }
        }

        if (collision.CompareTag("Weapon"))
        {
            
            if (collision.attachedRigidbody.sharedMaterial == null && col.sharedMaterial == null)
            {
                return;
            }

            WeaponAttachCheck otherAtachCheck = collision.GetComponent<WeaponAttachCheck>();

            if (collidedBodies.Contains(otherAtachCheck))
            {
                collidedBodies.Remove(otherAtachCheck);
            }

            CheckIfConnected();

            //connected_body_count--;

            //if (connected_body_count <= 0)
            //{
            //    connected_body_count = 0;

            //    if (!is_first_part)
            //    {
            //        SetConnected(false);
            //    }
            //}
        }
    }

    public void SetConnected(bool value)
    {
        //green_outline.SetActive(value);
        //red_outline.SetActive(!value);

        if (value)
        {
            col.sharedMaterial = default_friction;

            sprite.color = new Color(1, 1, 1, 1);
        }
        else
        {
            col.sharedMaterial = null;

            sprite.color = new Color(1, 1, 1, alpha_when_disconnected);
        }

        is_connected = value;
    }
}
