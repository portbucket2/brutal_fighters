using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Custom Enemy", menuName = "Custom Enemy", order = 54)]
public class CustomEnemy : ScriptableObject
{
    public LimbLength upperBody;
    public LimbLength lowerBody;
    public HeadSkin head;
    public TorsoSkin torso;
}


public enum LimbLength
{
    M = 0,
    L = 1,
    XL = 2,
}

public enum HeadSkin
{
    Default = 0,
    Circle = 1,
    Horn = 2,
    RookHead = 3,
    TriangleHead = 4,
    U_Shape = 5,
}

public enum TorsoSkin
{
    Default = 0,
    ChestHole = 1,
    FlabbyBelly = 2,
    RookTorso = 3,
    TriangleTorso = 4,
    I_Shape = 5,
}