using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial_CraftToCharacterShop : Tutorial
{
    public UnityEvent go_to_character_shop;

    bool condition;

    private void Start()
    {
        TeachGoToCharacterShop();
    }

    public void TeachGoToCharacterShop()
    {
        //condition = GameData.Instance.player_knows_weapon_buying == 0;
        condition = LevelDataController.currentFakeLevel == 3 && GameData.Instance.player_knows_limb_upgrade == 0;

        if (condition)
        {
            go_to_character_shop?.Invoke();
        }
    }
}
