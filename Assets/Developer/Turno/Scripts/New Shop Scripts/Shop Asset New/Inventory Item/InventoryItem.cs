using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InventoryItem", menuName = ConstantManager.GAME_NAME + "/Inventory/InventoryItem")]
public class InventoryItem : ScriptableObject
{
    #region Custom Variables
    public enum CharacterUnlockMethod
    {
        Ads,
        Coins,
        Free
    }
    #endregion

    #region Public Variables
    public int ItemIndexInInventoryManager { get; private set; }


    #endregion
    #region Private Variables
    [SerializeField] private bool isItemUnlockableThrowProgression;
    [SerializeField] private int requiredNumberOfLevelToUnlock;

    [SerializeField] private CharacterUnlockMethod characterUnlockMethod;
    [SerializeField] private int unlockCost;

    [SerializeField] private int itemId;
    [SerializeField] private int itemName;
    [SerializeField] private Sprite itemIcon;
    #endregion
}
