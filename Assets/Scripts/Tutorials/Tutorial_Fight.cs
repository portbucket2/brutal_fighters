using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial_Fight : Tutorial
{
    public UnityEvent on_double_tap_done;
    public UnityEvent on_drag_tut1_done;
    public UnityEvent on_drag_tut2_done;

    private int double_tap_done = 0;
    private int drag_done = 0;
    private bool drag_reminder = false;
    private bool drag_tut_objects_shown_once = false;

    public Action OnDragTutDoneEvent;
    bool drag_tut_done = false;
    bool show_tutorial = false;

    public List<GameObject> dragTutObjects;

    private GameManager game_manager;

    private int wiggle_count = 0;
    private int last_wiggle_dir = -1;
    private Vector3 last_mousepos = Vector3.zero;

    private void Awake()
    {
        show_tutorial = InvokeLevelSpecificEvents();
    }

    private void Start()
    {
        game_manager = FindObjectOfType<GameManager>();

        game_manager.OnLevelComplete += DragTutDone;

        gameObject.SetActive(show_tutorial);

        if (show_tutorial)
        {
            FindObjectOfType<GameManager>().StartFight();
        }
    }

    public void DoubleTapDone()
    {
        if (double_tap_done >= 1)
        {
            return;
        }
        
        double_tap_done++;
        
        if (double_tap_done == 1)
        {
            on_double_tap_done?.Invoke();
        }
    }

    public void DragDone()
    {
        drag_reminder = true;

        //if (double_tap_done < 1)
        //{
        //    return;
        //}

        //if (drag_done >= 2)
        //{
        //    return;
        //}

        //drag_done++;

        //if (drag_done == 1)
        //{
        //    on_drag_tut1_done?.Invoke();
        //}
        //if (drag_done == 2)
        //{
        //    on_drag_tut2_done?.Invoke();
        //}

        //Invoke("DelayedDragTutDone", 1f);
    }

    //void DelayedDragTutDone()
    //{
    //    drag_tut_hidden = true;

    //    OnDragTutDoneEvent?.Invoke();
    //}

    float idle_time_counter = 3f;

    private void Update()
    {
        if (double_tap_done < 1)
        {
            return;
        }

        //if (drag_done < 2)
        //{
        //    return;
        //}

        if (game_manager.level_completed)
        {
            return;
        }

        if (wiggle_count > 7)
        {
            wiggle_count = 0;


            if (drag_done < 2)
            {
                drag_done++;


                if (drag_done == 1)
                {
                    on_drag_tut1_done?.Invoke();
                }
                if (drag_done == 2)
                {
                    on_drag_tut2_done?.Invoke();
                }
            }

        }


        if (Input.GetMouseButton(0))
        {
            idle_time_counter = 3f;

            if (drag_reminder && drag_done >= 2)
            {
                drag_tut_objects_shown_once = false;

                if (!drag_tut_done)
                {
                    drag_tut_done = true;
                    OnDragTutDoneEvent?.Invoke();
                }
            }


            Vector3 touch_relative = last_mousepos - Input.mousePosition;

            if (Mathf.Sign(touch_relative.normalized.x) == Mathf.Sign(last_wiggle_dir))
            {
                last_wiggle_dir *= -1;

                wiggle_count++;


                if (drag_done < 1)
                {
                    if (Input.mousePosition.x < Screen.width / 2)
                    {
                        wiggle_count = 0;
                    }
                }
                else if (drag_done < 2)
                {
                    if (Input.mousePosition.x > Screen.width / 2)
                    {
                        wiggle_count = 0;
                    }
                }
            }
        }


        if (idle_time_counter <= 0)
        {
            if (!drag_tut_objects_shown_once)
            {
                drag_tut_objects_shown_once = true;
                drag_reminder = false;
                ShowDragTutObjects();
            }
        }
        else
        {
            idle_time_counter -= Time.deltaTime;
        }
        
        //Global.Instance.DebugText(wiggle_count.ToString());

        last_mousepos = Input.mousePosition;
    }

    private void ShowDragTutObjects()
    {
        foreach (GameObject item in dragTutObjects)
        {
            item.SetActive(true);
        }
    }

    private void DragTutDone()
    {
        OnDragTutDoneEvent?.Invoke();
    }
}
